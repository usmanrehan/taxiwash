//
//  StringToHTML+Extension.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/23/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import Foundation


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }}
