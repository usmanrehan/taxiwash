//
//  BaseMenuController.swift
//  ArkanDubai
//
//  Created by Khawar Islam on 21/11/2017.
//  Copyright © 2017 Khawar Islam. All rights reserved.
//

import UIKit
import LGSideMenuController

class BaseMenuController: UIViewController {
    
    let preferredLanguage = Locale.preferredLanguages[0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if preferredLanguage.contains("en") {
            addMenuBarLeftButtonItem()
            addMNotificationBarRightButtonItem()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Add Buttons In Navigation Bar
    func addMNotificationBarRightButtonItem() {
        let image = UIImage(named: "notification")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.didTapNotification))
        
        self.navigationItem.rightBarButtonItem = backItem
    }
    
    @objc func didTapNotification(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
        vc.flag = true
        navigationController?.pushViewController(vc,animated: true)
    }
    
    func showRightView(sender: AnyObject?) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    func addMenuBarLeftButtonItem() {
        let image = UIImage(named: "menu")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.openLeftView(_:)))
        
        self.navigationItem.leftBarButtonItem = backItem
        
    }
}
