//
//  BaseController.swift
//  ArkanDubai
//
//  Created by Khawar Islam on 18/11/2017.
//  Copyright © 2017 Khawar Islam. All rights reserved.
//

import UIKit
import LGSideMenuController

class BaseController: UIViewController {
    
    var window: UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addMNotificationBarRightButtonItem()
        
        guard (self.navigationController != nil) else {
            return
        }
        
        if (self.navigationController?.viewControllers.count)! > 1 {
            self.addBackBarButtonItem()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addBackBarButtonItem() {
        let image = UIImage(named: "NavBackButton")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
    
  @objc func onBtnBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // Add Buttons In Navigation Bar
    func addMNotificationBarRightButtonItem() {
        let image = UIImage(named: "notification")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.didTapNotification))
        
        self.navigationItem.rightBarButtonItem = backItem
    }
    
    @objc func didTapNotification(){
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
        navigationController?.pushViewController(vc,animated: true)
    }
    
    

}
