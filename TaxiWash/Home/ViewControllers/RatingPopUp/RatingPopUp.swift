//
//  RatingPopUp.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import Cosmos
class RatingPopUp: UIView {

    @IBOutlet weak var viewServiceRating: CosmosView!
    
    var orderId = 0

    var ratingPopUp : RatingPopUp!
    
    @IBAction func didTapRating(_ sender: Any) {
        RatingWebService()
    }
    
    
    
    func RatingWebService(){
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            print(result)
            self.removeFromSuperview()
            Utility.hideLoader()
        }
        let parameters: Dictionary<String, Any> = [
            "order_id":orderId,
            "rate" : Int(viewServiceRating.rating)
        ]
        
        print(parameters)
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.postRequestUserParameter(route: Route.OrderFeedback.rawValue, parameters: parameters, success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
}
