//
//  ExpandServiceNibTableViewCellTableViewCell.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/26/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import SDWebImage

class ExpandServiceNibTableViewCellTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var labDescription: UILabel!
    @IBOutlet weak var pageControlServices: UIPageControl!
    @IBOutlet weak var collectionViewCarosel: UICollectionView!
   var arrServices : [ServiceDetail] = Array()
    var selectedIndexProduct = 0
    override func awakeFromNib() {
     
        collectionViewCarosel.register(UINib(nibName: "ServiceImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "imageCarosuel")
        collectionViewCarosel.delegate = self
        collectionViewCarosel.dataSource = self
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        pageControlServices.numberOfPages = (self.arrServices[selectedIndexProduct].service_images.count)
        return (self.arrServices[selectedIndexProduct].service_images.count)
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = collectionViewCarosel.frame.size.width
        pageControlServices?.currentPage = Int(collectionViewCarosel.contentOffset.x / pageWidth)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewCarosel.dequeueReusableCell(withReuseIdentifier: "imageCarosuel", for: indexPath) as! ServiceImageCollectionViewCell
        if arrServices[selectedIndexProduct].service_images[indexPath.row].service_image != nil {
        let url = URL(string:  arrServices[selectedIndexProduct].service_images[indexPath.row].service_image!)
        cell.imageViewService.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionViewCarosel.frame.size;
        
    }
}
