//
//  ServiceImageCollectionViewCell.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/26/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class ServiceImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewService: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
