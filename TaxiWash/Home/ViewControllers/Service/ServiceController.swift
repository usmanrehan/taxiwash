//
//  ServiceController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import ExpyTableView

class ServiceController: BaseController {
    
    @IBOutlet weak var tabeViewServices: ExpyTableView!
    
    var arrServices : [ServiceDetail] = Array()

    var selected_product = 0
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabeViewServices.delegate = self
        tabeViewServices.dataSource = self
        ServicesWebServices()
        self.title = "SERVICE"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension ServiceController : ExpyTableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  arrServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ExpandServiceNibTableViewCellTableViewCell", owner: self, options: nil)?.first as! ExpandServiceNibTableViewCellTableViewCell
        let view = UIView()
        
        cell.selectedBackgroundView = view
          cell.selectedIndexProduct = self.selected_product
        cell.labDescription.text = arrServices[selected_product].detail.htmlToString
        
        print(arrServices)
        cell.arrServices = arrServices
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 57
        }
        return 363
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ServiceTableViewCell", owner: self, options: nil)?.first as! ServiceTableViewCell
        cell.labServiceName.text = arrServices[section].title
        //cell.backgroundColor = AppConstants.HomeLogoColor
        let view = UIView()
        cell.selectedBackgroundView = view
        return cell
    }
    
    func ServicesWebServices()  {
        let successClosure: DefaultResultArrayAPISuccessClosure = {
            (result) in
            print(result)
            self.arrServices.removeAll()
            for res in result {
                self.arrServices.append(ServiceDetail(value: res))
            }
            self.tabeViewServices.reloadData()
            print(self.arrServices)
            Utility.hideLoader()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.getServicesWith(success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    
}


extension ServiceController : ExpyTableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tabeViewServices.deselectRow(at: indexPath, animated: true)
        print("section no" + "\(indexPath.section)")
        print("row no" + "\(indexPath.row)")
    }
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
            
            self.selected_product = section
        case .willCollapse:
            print("WILL COLLAPSE")
           // arrImages.removeAll()
        case .didExpand:
            self.tabeViewServices.reloadData()
            for i in 0..<self.arrServices.count{
                if i != section{
                    tabeViewServices.collapse(i)
                }
            }
        case .didCollapse:
            print("DID COLLAPSE")
        }
    }
    
}

