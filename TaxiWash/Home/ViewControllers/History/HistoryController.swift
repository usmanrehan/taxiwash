//
//  HistoryController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class HistoryController: BaseMenuController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var btnPending: UIButton!
    @IBOutlet weak var btnInProgress: UIButton!
    @IBOutlet weak var btnComplete: UIButton!
    @IBOutlet weak var btnCancelled: UIButton!
     @IBOutlet weak var tablViewHistory: UITableView!
    var arrHistoryPending : [History] = Array()
    
    var btnCancel = UIButton()
    var ratingPopUp : RatingPopUp!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "HISTORY"
        btnPending.backgroundColor = AppConstants.HomeLogoColor
        self.ratingPopUp = Bundle.main.loadNibNamed("RatingPopUp", owner: self, options: nil)?.last as? RatingPopUp
        self.ratingPopUp.frame = CGRect(x: 0, y: 0 , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashNotificationKey, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Connectivity.isConnectedToInternet() {
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                HistoryWebService(arrServiceStatus: "pending")
            }
           
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("CreateWashNotificationKey")
    }
    
    @objc func notificationReceived(notification:NSNotification)    {
        self.view.window?.addSubview(self.ratingPopUp)
        let orderID =  notification.userInfo?["order_id"] as! Int
        self.ratingPopUp.orderId = orderID
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistoryPending.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryTableViewCell
        cell.labDate.text = arrHistoryPending[indexPath.row].date
           cell.labTime.text = arrHistoryPending[indexPath.row].time
        
        cell.labOrderID.text = String(arrHistoryPending[indexPath.row].id)
        cell.labLocation.text = arrHistoryPending[indexPath.row].location
        
        var arrServiceNam = Array<String>()
        for ssNAme in  arrHistoryPending[indexPath.row].order_services {
            arrServiceNam.append((ssNAme.service_detail?.title)!)
        }
        let str_Services_Comma = arrServiceNam.joined(separator: ",")
        print(str_Services_Comma)


       cell.labService.text = str_Services_Comma
        
    
        
        let view = UIView()
        cell.selectedBackgroundView = view
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    @IBAction func didTapPending(_ sender: Any) {
        tablViewHistory.allowsSelection = true
        if Connectivity.isConnectedToInternet() {
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                
                HistoryWebService(arrServiceStatus: "pending")
            }
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
            
        }
        
        btnPending.backgroundColor = AppConstants.HomeLogoColor
        btnInProgress.backgroundColor = AppConstants.HistoryBackgournColor
        btnComplete.backgroundColor = AppConstants.HistoryBackgournColor
        btnCancelled.backgroundColor = AppConstants.HistoryBackgournColor
    }
    
    @IBAction func didTapProgress(_ sender: Any) {
       tablViewHistory.allowsSelection = false
        if Connectivity.isConnectedToInternet() {
            
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                 HistoryWebService(arrServiceStatus: "inprogress")
                btnCancel.isHidden = true
            }
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
            
        }
        btnInProgress.backgroundColor = AppConstants.HomeLogoColor
        btnPending.backgroundColor = AppConstants.HistoryBackgournColor
        btnComplete.backgroundColor = AppConstants.HistoryBackgournColor
        btnCancelled.backgroundColor = AppConstants.HistoryBackgournColor
    }
    
    @IBAction func didTapCompleted(_ sender: Any) {
          tablViewHistory.allowsSelection = false
        if Connectivity.isConnectedToInternet() {
           
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                HistoryWebService(arrServiceStatus: "completed")
            }
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
            
        }
        btnComplete.backgroundColor = AppConstants.HomeLogoColor
        btnCancelled.backgroundColor = AppConstants.HistoryBackgournColor
        btnInProgress.backgroundColor = AppConstants.HistoryBackgournColor
        btnPending.backgroundColor = AppConstants.HistoryBackgournColor
        
    }
    
    @IBAction func didTapCancelled(_ sender: Any) {
        tablViewHistory.allowsSelection = false
        if Connectivity.isConnectedToInternet() {
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                  HistoryWebService(arrServiceStatus: "cancel")
            }
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
        btnCancelled.backgroundColor = AppConstants.HomeLogoColor
        btnPending.backgroundColor = AppConstants.HistoryBackgournColor
        btnInProgress.backgroundColor = AppConstants.HistoryBackgournColor
        btnComplete.backgroundColor = AppConstants.HistoryBackgournColor
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CancelController") as! CancelController
   
        vc.arrHistoryPending = arrHistoryPending[indexPath.row]
        navigationController?.pushViewController(vc,animated: true)
    }
    
    
    func HistoryWebService(arrServiceStatus: String){
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            let resPending = result[arrServiceStatus] as! NSArray
            print(resPending)
            self.arrHistoryPending.removeAll()
            for washHistory in resPending {
                self.arrHistoryPending.append(History(value: washHistory))
            }
            self.tablViewHistory.reloadData()
            print(self.arrHistoryPending)
            Utility.hideLoader()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.getOrderHistory(success: successClosure, failure: { (error) in
           Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
            Utility.hideLoader()
        })
    }
    
    
}
