//
//  CancelController.swift
//  TaxiWash
//
//  Created by Khawarislam on 3/1/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class CancelController: BaseController{
    
   
    @IBOutlet weak var labPayment: UILabel!
    @IBOutlet weak var labServices: UILabel!
 
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var labTime: UILabel!
    @IBOutlet weak var labDate: UILabel!
    @IBOutlet weak var labAmount: UILabel!
    
    @IBOutlet weak var labServicesHeadingLine: UILabel!
    @IBOutlet weak var labOrderID: UILabel!
    
    var TotalAmount = 0
      var arrHistoryPending = History()
    override func viewDidLoad() {
        super.viewDidLoad()

      self.title = "HISTORY"
        labDate.text = arrHistoryPending.date  ?? ""
        labTime.text = arrHistoryPending.time ?? ""

        var arrServiceNam = Array<String>()
        for ssNAme in  arrHistoryPending.order_services {
            arrServiceNam.append((ssNAme.service_detail?.title)!)
        }
        let str_Services_Comma = arrServiceNam.joined(separator: ",")
        labServicesHeadingLine.text = str_Services_Comma
        labServices.text = str_Services_Comma
        
        for amount in  arrHistoryPending.order_services {
            TotalAmount = TotalAmount + Int((amount.service_detail?.price)!)!
        }
        labAmount.text = "$" + String(TotalAmount)
        labPayment.text = "Cash"
        labOrderID.text = "#" + String(arrHistoryPending.id)
        
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 
           
    

    @IBAction func didTApCancel(_ sender: Any) {
        CancelOrderWebService(OrderId: arrHistoryPending.id)
    }
    
    
    func CancelOrderWebService(OrderId:Int)  {
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            print(result)
            Utility.hideLoader()
            _ = self.navigationController?.popViewController(animated: true)
            
            
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.CancelInProgressOrder(OrderID: OrderId, success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    

}
