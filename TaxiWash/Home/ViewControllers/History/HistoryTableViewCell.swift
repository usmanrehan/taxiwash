//
//  HistoryTableViewCell.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var labOrderID: UILabel!
    @IBOutlet weak var labDate: UILabel!
    @IBOutlet weak var labService: UILabel!
    @IBOutlet weak var labLocation: UILabel!
    @IBOutlet weak var labCategory: UILabel!
    @IBOutlet weak var labTime: UILabel!
    @IBOutlet weak var btnHistory: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
