//
//  CancelTableViewCell.swift
//  TaxiWash
//
//  Created by Khawarislam on 3/1/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class CancelTableViewCell: UITableViewCell {

    
   
    @IBOutlet weak var labPayment: UILabel!
    
    
    @IBOutlet weak var labServices: UILabel!
    
   
    @IBOutlet weak var labAmount: UILabel!

    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var labTime: UILabel!
    
    @IBOutlet weak var labDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
