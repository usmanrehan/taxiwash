//
//  NotificationController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class NotificationController: BaseMenuController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewNotification: UITableView!
    var dataSource : [Notification] = Array()
    var flag = true
    
     var ratingPopUp : RatingPopUp!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Notifications"
        self.ratingPopUp = Bundle.main.loadNibNamed("RatingPopUp", owner: self, options: nil)?.last as? RatingPopUp
        self.ratingPopUp.frame = CGRect(x: 0, y: 0 , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashNotificationKey, object: nil)
         self.navigationItem.rightBarButtonItem = nil
       
        
        if Connectivity.isConnectedToInternet() {
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
               UserNotificationWebService()
            }
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
        guard (self.navigationController != nil) else {
            return
        }
        
        if flag == true {
            if (self.navigationController?.viewControllers.count)! > 1 {
                self.addBackBarButtonItem()
            }
        }
    }
    
   
  
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("CreateWashNotificationKey")
    }
    @objc func notificationReceived(notification:NSNotification)    {
        self.view.window?.addSubview(self.ratingPopUp)
        let orderID =  notification.userInfo?["order_id"] as! Int
        self.ratingPopUp.orderId = orderID
    }
    
    
    
    func addBackBarButtonItem() {
        let image = UIImage(named: "NavBackButton")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
    
    @objc func onBtnBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableViewNotification.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
    
        cell.labNotification.text = "\(String(describing: dataSource[indexPath.row].message!))"

        
        let view = UIView()
        cell.selectedBackgroundView = view
        
        return cell
    }
    
    func UserNotificationWebService()                                 {
        let successClosure: DefaultResultArrayAPISuccessClosure = {
            (result) in
            print(result)
            self.dataSource.removeAll()
            
            print(result)
            for orderList in result {
                print(orderList)
                self.dataSource.append(Notification(value: orderList))
            }
            Utility.hideLoader()
            self.tableViewNotification.reloadData()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.GetNotification(success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
            Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
        })
    }


}
