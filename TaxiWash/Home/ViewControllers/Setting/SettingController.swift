//
//  SettingController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class SettingController: BaseMenuController {

    @IBOutlet weak var swtichNotification: UISwitch!
     var ratingPopUp : RatingPopUp!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SETTINGS"
        swtichNotification.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashNotificationKey, object: nil)
        self.ratingPopUp = Bundle.main.loadNibNamed("RatingPopUp", owner: self, options: nil)?.last as? RatingPopUp
        self.ratingPopUp.frame = CGRect(x: 0, y: 0 , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if AppStateManager.sharedInstance.loggedInUser?.push_status == 1 {
            self.swtichNotification.isOn = true
            
        } else {
            
        }
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("CreateWashNotificationKey")
    }
    @objc func notificationReceived(notification:NSNotification)    {
        self.view.window?.addSubview(self.ratingPopUp)
        let orderID =  notification.userInfo?["order_id"] as! Int
        self.ratingPopUp.orderId = orderID
    }
    
    @IBAction func didTapPrivacy(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PrivacyController") as! PrivacyController
        navigationController?.pushViewController(vc,animated: true)
        
    }
    
    @IBAction func didTapAboutUs(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AboutUsController") as! AboutUsController
        navigationController?.pushViewController(vc,animated: true)
        
    }
    @IBAction func didTapChangePassword(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        navigationController?.pushViewController(vc,animated: true)
        
    }
    
    @IBAction func didTapSwtich(_ sender: Any) {
        if swtichNotification.isOn {
            if Connectivity.isConnectedToInternet() {
                if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                    Utility.alert(title: "Alert", Message: "Please Login")
                } else {
                     ChangePushNotificationWebService(Status: 1)
                }
              
            } else  {
                Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
            }
            
        } else{
            if Connectivity.isConnectedToInternet() {
                
                if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                    Utility.alert(title: "Alert", Message: "Please Login")
                } else {
                   ChangePushNotificationWebService(Status: 0)
                }
            } else  {
                Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
            }
        }
    }
    
    func ChangePushNotificationWebService(Status:Int) {
        
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
          Utility.hideLoader()
            
            print(result)
            let token = AppStateManager.sharedInstance.loggedInUser.token
            AppStateManager.sharedInstance.loggedInUser = User(value: result)
            try! Global.APP_REALM?.write(){
                AppStateManager.sharedInstance.loggedInUser.token = token
                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
            }
            
            
            
        }
       Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.ChangePushNotificationStatus(PushStatus: Status,success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
            Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
        })
    }
    
    

}
