//
//  ExpressDetailsController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/20/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class ExpressDetailsController: BaseController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableViewExpressDetails: UITableView!
    
    let tags = ["This", "is", "a", "example", "of", "Cloud", "Tag", "View"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell :ExteriorTableViewCell!
        let view = UIView()
        
        
        switch indexPath.row {
        case 0:
            cell = tableViewExpressDetails.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            return cell
        case 1:
            cell = tableViewExpressDetails.dequeueReusableCell(withIdentifier: "dateCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            return cell
        case 2:
            cell = tableViewExpressDetails.dequeueReusableCell(withIdentifier: "timeCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            return cell
        case 3:
            cell = tableViewExpressDetails.dequeueReusableCell(withIdentifier: "cashCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            return cell
        case 4:
            cell = tableViewExpressDetails.dequeueReusableCell(withIdentifier: "serviceTagCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            for tag in tags {
                // cell.tagListService.tags.append(TagView(text: tag))
                
            }
            cell.tagListService.tintColor = UIColor.gray
            return cell
        case 5:
            cell = tableViewExpressDetails.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            return cell
        default:
            print("fail")
        }
        
        
        return cell
    }
    
}
