//
//  ExteriorTableViewCell.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/19/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import TagListView
import AAPickerView
import DropDown
import GooglePlacePicker

protocol DatePickerSelected {
    func didDatePick(dateString: String)
    func didTimePick(timeString: String)
    func didRemoveTag(tagIndex indexPath:Int)
}



class ExteriorTableViewCell: UITableViewCell,TagListViewDelegate  {
    
    
    @IBOutlet weak var btnCash: UIButton!
    @IBOutlet weak var btnAddService: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tagListService: TagListView!
    @IBOutlet weak var txtFieldDate: AAPickerView!
    @IBOutlet weak var txtFieldTime: AAPickerView!
    @IBOutlet weak var labservices: UILabel!
    @IBOutlet weak var labCost: UILabel!
    
    var delegate: DatePickerSelected?
    var date24 : String! = ""
    var selectedCategory:[Categories]!
      var todayDate = Date()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureTags(){
        
        tagListService.removeAllTags()
        tagListService.enableRemoveButton = true
        let arrStringTags = self.selectedCategory.map { (category) -> String in
            return category.title!
        }
        tagListService.addTags(arrStringTags)
        
    }
    
    @objc func didTapDate() {
        txtFieldDate.pickerType = .DatePicker
        txtFieldDate.toolbar.barTintColor = UIColor.lightGray
        
        let datePicker = txtFieldDate.datePicker
        datePicker?.datePickerMode = .date
        datePicker?.minimumDate = Date()
        
        txtFieldDate.dateDidChange = { date in
             let dateFormatter = self.txtFieldDate.dateFormatter
             dateFormatter.dateFormat = "MMM dd yyyy"

            let dateString = dateFormatter.string(from: date)
            
            self.txtFieldDate.text = dateString
            
            self.delegate?.didDatePick(dateString: dateString)
        }
    }
    
    @objc func ToTimepicker(){
        txtFieldTime.pickerType = .DatePicker
        txtFieldTime.toolbar.barTintColor = UIColor.lightGray
        
        let datePicker = txtFieldTime.datePicker
        datePicker?.datePickerMode = .time
        
        txtFieldTime.dateDidChange = { date in
            let dateformatter = self.txtFieldTime.dateFormatter
            dateformatter.dateFormat = "HH:mm:ss"

            let timeString = dateformatter.string(from: date)
            
            self.txtFieldTime.text = timeString
            self.delegate?.didTimePick(timeString: timeString)
        }
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        var index = 0
        for (i , iTagView) in tagListService.tagViews.enumerated() {
            if tagView == iTagView {
                index = i
                delegate?.didRemoveTag(tagIndex: index)
            }
        }
        sender.removeTag(title)
        sender.removeTagView(tagView)
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        
    }
    
    
}

class Time: Comparable, Equatable {
    init(_ date: Date) {
        //get the current calender
        let calendar = Calendar.current
        
        //get just the minute and the hour of the day passed to it
        let dateComponents = calendar.dateComponents([.hour, .minute], from: date)
        
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = dateComponents.hour! * 3600 + dateComponents.minute! * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        hour = dateComponents.hour!
        minute = dateComponents.minute!
    }
    
    init(_ hour: Int, _ minute: Int) {
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = hour * 3600 + minute * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        self.hour = hour
        self.minute = minute
    }
    
    var hour : Int
    var minute: Int
    
    var date: Date {
        //get the current calender
        let calendar = Calendar.current
        
        //create a new date components.
        var dateComponents = DateComponents()
        
        dateComponents.hour = hour
        dateComponents.minute = minute
        
        return calendar.date(byAdding: dateComponents, to: Date())!
    }
    
    /// the number or seconds since the beggining of the day, this is used for comparisions
    private let secondsSinceBeginningOfDay: Int
    
    //comparisions so you can compare times
    static func == (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay == rhs.secondsSinceBeginningOfDay
    }
    
    static func < (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay < rhs.secondsSinceBeginningOfDay
    }
    
    static func <= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay <= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func >= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay >= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func > (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay > rhs.secondsSinceBeginningOfDay
    }
}

extension Date {
    var time: Time {
        return Time(self)
    }
}
