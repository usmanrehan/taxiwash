//
//  ExteriorController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import TagListView
import GooglePlaces
import DropDown
import GooglePlacePicker


class ExteriorController: BaseController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,GMSPlacePickerViewControllerDelegate{
    
    
    @IBOutlet weak var tableViewExterior: UITableView!
    
    var WashRequestPopUp: WashRequestPopUp!
    var objectSelectedService = Categories()
    
    var Address : String = "Address"
    var Latitude : String = ""
    var Longitude : String = ""
    var arrServices : [Categories] = Array()
    let dropDown = DropDown()
    
    
    var arrServicesTitle = Array<String>()
    var arrSelectedServices : [Categories] = Array()
    var date : String = ""
    var dateFormat : String = ""
    var time = ""
    
    var selectedDate: String?
    var selectedTime: String?
    
    //var paymentType = "Cash"
    var TotalCost = 0
    var str_ServicesID_Comma = ""
    var arrServiceSelectedIDS = Array<Int>()
    var arrSelectedTags = [Categories]()
    
    var dropdownPaymentType = DropDown()
    var dropdwonPaymentLabel : String! = ""
    var arrPaymentType = ["Cash","Card"]
    var placesClient: GMSPlacesClient!
    var flag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placesClient = GMSPlacesClient.shared()
        ServicesWebServices()
        self.dropdownPaymentType.dataSource = arrPaymentType
        self.dropdownPaymentType.direction = .any
        self.title = objectSelectedService.title
        dropDown.direction = .any
        
        self.WashRequestPopUp = Bundle.main.loadNibNamed("WashRequestPopUp", owner: self, options: nil)?.last as? WashRequestPopUp
        self.WashRequestPopUp.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        //NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashOrderNotification, object: nil)
        self.WashRequestPopUp.btnOK.addTarget(self, action: #selector(notificationReceived(_:)), for: .touchUpInside)
        // Array for Title and Cost
        
        for ii in self.objectSelectedService.category_services {
            self.arrServicesTitle.append((ii.service_detail?.title)!)
            self.TotalCost = self.TotalCost + Int((ii.service_detail?.price)!)!
            arrServiceSelectedIDS.append((ii.service_detail?.id)!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return 50
        case 2:
            return 50
        case 3:
            return 50
        default:
            print("fail")
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableViewExterior.deselectRow(at: indexPath, animated: true)
        tableViewExterior.reloadData()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell :ExteriorTableViewCell!
        let view = UIView()
        
        
        switch indexPath.row {
        case 0:
            cell = tableViewExterior.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            cell.labCost.text =  "$" + String(TotalCost)
            var arrServiceNam = Array<String>()
            for ssNAme in objectSelectedService.category_services {
                arrServiceNam.append((ssNAme.service_detail?.title)!)
            }
            let str_Services_Comma = arrServiceNam.joined(separator: ", ")
            cell.labservices.text = str_Services_Comma
            return cell
        case 1:
            cell = tableViewExterior.dequeueReusableCell(withIdentifier: "dateCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            cell.txtFieldDate.text = self.date
            cell.delegate = self
            cell.didTapDate()
            return cell
        case 2:
            cell = tableViewExterior.dequeueReusableCell(withIdentifier: "timeCell", for: indexPath) as! ExteriorTableViewCell
            cell.delegate = self
            cell.ToTimepicker()
            cell.txtFieldTime.text! = self.time
            cell.selectedBackgroundView = view
            return cell
        case 3:
            cell = tableViewExterior.dequeueReusableCell(withIdentifier: "cashCell", for: indexPath) as! ExteriorTableViewCell
            cell.btnCash.addTarget(self, action: #selector(didTapPaymentType), for: .touchUpInside)
            dropdownPaymentType.anchorView = cell.btnCash
            self.dropdownPaymentType.selectionAction = { [unowned self] (index: Int, item: String) in
                cell.btnCash.setTitle(item, for: .normal)
                self.dropdwonPaymentLabel = item
            }
            cell.selectedBackgroundView = view
            return cell
        case 4:
            cell = tableViewExterior.dequeueReusableCell(withIdentifier: "serviceTagCell", for: indexPath) as! ExteriorTableViewCell
            cell.selectedBackgroundView = view
            cell.btnAddService.addTarget(self, action: #selector(didTapAddServices), for: .touchUpInside)
            dropDown.anchorView = cell.btnAddService
            cell.tagListService.delegate = cell.self
            cell.selectedCategory = self.arrSelectedTags
            cell.delegate = self
            cell.configureTags()
            return cell
        case 5:
            cell = tableViewExterior.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! ExteriorTableViewCell
            cell.btnLocation.addTarget(self, action: #selector(didTapLocation), for: .touchUpInside)
            cell.btnContinue.addTarget(self, action: #selector(didTapOrder), for: .touchUpInside)
            cell.btnLocation.setTitle(Address, for: .normal)
            cell.selectedBackgroundView = view
            return cell
        default:
            print("fail")
        }
        return cell
    }
    
    @objc func didTapLocation () {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
                UINavigationBar.appearance().tintColor = UIColor.black
                UINavigationBar.appearance().barTintColor = UIColor.black
          UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black, NSAttributedStringKey.font: UIFont.init(name: "BebasNeueBold", size: 24)!]
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
          UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font: UIFont.init(name: "BebasNeueBold", size: 24)!]
     
        if place.formattedAddress != nil {
            Address = place.formattedAddress!
            Latitude = String(place.coordinate.latitude)
            Longitude = String(place.coordinate.longitude)
            tableViewExterior.reloadData()
        } else {
            Address = place.name
            tableViewExterior.reloadData()
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font: UIFont.init(name: "BebasNeueBold", size: 24)!]
        viewController.dismiss(animated: true, completion: nil)
    }
    
   
    private func validateDateTime() -> Bool {
        guard self.selectedDate != nil else {
            return false
        }
        guard self.selectedTime != nil else {
            return false
        }
        let dateTimeString = String(format: "%@ %@", self.selectedDate!, self.selectedTime!)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy HH:mm:ss"
        let dateTime = formatter.date(from: dateTimeString)
        let currentDateTime = Date()
        if currentDateTime > dateTime! {
            return false
        }
        return true
    }
    
    
    @objc func didTapAddServices () {
        dropDown.show()
    }
    
    @objc func didTapPaymentType(){
        dropdownPaymentType.show()
    }
    
    @objc func didTapOrder () {
        
        if date.count == 0 {
            Utility.alert(title: "Alert", Message: "Please select date")
        }
        else {
            if time.count == 0 {
                Utility.alert(title: "Alert", Message: "Please select time")
            } else {

                guard self.validateDateTime() else {
                    Utility.alert(title: "Error", Message: "Date time is not valid")
                    return
                }
                
                self.view.window?.addSubview(self.WashRequestPopUp)
            }
        }
    }
    
    @objc func notificationReceived(_ sender: UIButton)    {
        tableViewExterior.reloadData()
        let stringArray = arrServiceSelectedIDS.map
        {
            String($0)
        }
        str_ServicesID_Comma = stringArray.joined(separator: ",")
        if Connectivity.isConnectedToInternet() {
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                self.WashRequestPopUp.removeFromSuperview()
                if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "GuestRequestViewController") as? GuestRequestViewController {
                    viewController.objectSelectedService = objectSelectedService
                    viewController.date = dateFormat
                    viewController.time = time
                    viewController.Address = Address
                    viewController.Latitude = Latitude
                    viewController.Longitude = Longitude
                    viewController.dropdwonPaymentLabel = dropdwonPaymentLabel
                    viewController.str_ServicesID_Comma = str_ServicesID_Comma
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else {
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "MMM dd yyyy"
                
                let dateformatterWebService = DateFormatter()
                dateformatterWebService.dateFormat = "yyyy-MM-dd"
                
                if let date = dateFormatterGet.date(from: self.date){
                    let dateServiceFormat = dateformatterWebService.string(from: date)
                    self.dateFormat = dateServiceFormat
                }
                else {
                    print("There was an error decoding the string")
                }
                
//                let date = formatter.date(from: self.date)
//                self.date = formatter.string(from: date!)
                
                CreateOrderWebservice()
            }
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
    }
    
    
    func CreateOrderWebservice()  {
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            print(result)
            self.WashRequestPopUp.removeFromSuperview()
            _ = self.navigationController?.popToRootViewController(animated: true)
            Utility.AlertSucess(title: "Success", Message: "Thank you for request a service from TaxiWash")
            Utility.hideLoader()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.CreateOrderWith(CategoryID: objectSelectedService.id, Date: dateFormat, Time: time, PaymentType: dropdwonPaymentLabel, Location: Address, Latitude: Latitude, Longitude: Longitude, Total: String(TotalCost), ServiceIDs: str_ServicesID_Comma, success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    
    

    
    func ServicesWebServices()  {
        
       
        let successClosure: DefaultResultArrayAPISuccessClosure = {
            (result) in
            print(result)
            var arrString : [String] = []
            var arrFilteresedServices : [Categories] = Array()
            self.arrServices.removeAll()
            
            for i in result{
                let obj = Categories(value : i)
                self.arrServices.append(obj)
            }
            
            for i in 0..<self.arrServices.count{
                for j in self.objectSelectedService.category_services {
                    if j.service_detail?.id == self.arrServices[i].id{
                        self.arrServices[i].flag = true
                    }
                }
            }
            print(self.arrServices)
            
            for i in self.arrServices{
                if !i.flag{
                    arrString.append(i.title ?? "")
                    arrFilteresedServices.append(i)
                }
            }
            self.dropDown.dataSource = arrString
            
            self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                
                
                let category = arrFilteresedServices[index]
                
                if !self.arrSelectedTags.contains(category) {
                    self.arrSelectedTags.append(category)
                    
                    self.arrServiceSelectedIDS.append(category.id)
                    self.TotalCost = self.TotalCost + Int(category.price)!
                    self.tableViewExterior.reloadData()
                }
            }
            
            print(self.arrServices)
            Utility.hideLoader()
        }
        
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.getServicesWith(success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    
}


extension NSNotification.Name {
    public static let CreateWashNotification = NSNotification.Name(rawValue: "CreateWashNotification")
}

extension ExteriorController: DatePickerSelected {
    
    func didRemoveTag(tagIndex indexPath: Int) {
        let price = self.arrSelectedTags[indexPath].price
        self.TotalCost = self.TotalCost-Int(price!)!
        self.arrSelectedTags.remove(at: indexPath)
        self.tableViewExterior.reloadData()
    }
    
    func didDatePick(dateString: String) {
        self.selectedDate = dateString
        self.date = dateString
        
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateFormat = "MMM dd yyyy"
//
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
//
//        if let datee = dateFormatterGet.date(from: date){
//            dateFormat = dateFormatterPrint.string(from: datee)
//        }
        
        self.tableViewExterior.reloadData()
    }
    
    func didTimePick(timeString: String) {
        self.time = timeString
        self.selectedTime = timeString
        tableViewExterior.reloadData()
    }
    
}
