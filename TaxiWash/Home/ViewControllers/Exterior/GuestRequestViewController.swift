//
//  GuestRequestViewController.swift
//  TaxiWash
//
//  Created by Khawarislam on 4/13/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class GuestRequestViewController: BaseController {

    @IBOutlet weak var txtFieldFullName: UITextField!
    @IBOutlet weak var txtFieldMobileNumber: UITextField!
    
    var objectSelectedService = Categories()
    var TotalCost = 0
    var str_ServicesID_Comma = ""
    var arrServiceSelectedIDS = Array<Int>()
    
    var Address : String = "Address"
    var Latitude : String = ""
    var Longitude : String = ""
    var date : String = ""
    var time = ""
    var dropdwonPaymentLabel : String! = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.title = "GUEST REQUEST"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapRequestOrder(_ sender: UIButton)    {
        let stringArray = arrServiceSelectedIDS.map
        {
            String($0)
        }
        str_ServicesID_Comma = stringArray.joined(separator: ",")
       
        if txtFieldFullName.text?.count == 0 {
            Utility.alert(title: "Alert", Message: "Please enter fullname")
        } else {
            if txtFieldMobileNumber.text?.count == 0 {
                  Utility.alert(title: "Alert", Message: "Please enter phone number")
            } else {
                if Connectivity.isConnectedToInternet() {
                    CreateOrderWebservice()
                }
                else  {
                    Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
                }
            }
        }
      
    }
    func CreateOrderWebservice()  {
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            print(result)
              Utility.AlertSucess(title: "Success", Message: "Thank you for request a service from TaxiWash")
            Utility.hideLoader()
            Constants.APP_DELEGATE.changeRootViewController()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.CreateOrderRequestGuestWith(CategoryID: objectSelectedService.id, Date: date, Time: time, PaymentType: dropdwonPaymentLabel, Location: Address, Latitude: Latitude, Longitude: Longitude, Total: String(TotalCost), ServiceIDs: str_ServicesID_Comma, GuestUsername: txtFieldFullName.text!, GuestPhone: txtFieldMobileNumber.text!, success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    

}
