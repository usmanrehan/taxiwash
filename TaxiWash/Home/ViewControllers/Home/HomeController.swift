//
//  HomeController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/15/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class HomeController: BaseMenuController {
    
    var arrCategories = [Categories?]()
    
    @IBOutlet weak var btnExteriror: UIButton!
    @IBOutlet weak var btnFullCarWash: UIButton!
    @IBOutlet weak var btnExpressDetails: UIButton!
    @IBOutlet weak var btnServices: UIButton!
    
    var ratingPopUp : RatingPopUp!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashNotificationKey, object: nil)
        self.ratingPopUp = Bundle.main.loadNibNamed("RatingPopUp", owner: self, options: nil)?.last as? RatingPopUp
        self.ratingPopUp.frame = CGRect(x: 0, y: 0 , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CategoriesWebServices()
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("CreateWashNotificationKey")
    }
    @objc func notificationReceived(notification:NSNotification)    {
        self.view.window?.addSubview(self.ratingPopUp)
        let orderID =  notification.userInfo?["order_id"] as! Int
        self.ratingPopUp.orderId = orderID
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func didTapExterior(_ sender: UIButton) {
        if let object = arrCategories[sender.tag] {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ExteriorController") as! ExteriorController
            vc.objectSelectedService = object
            navigationController?.pushViewController(vc,animated: true)
        }
    }
    
    @IBAction func didTapFullCarWash(_ sender: UIButton) {
        if let objectCategory = arrCategories[sender.tag] {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ExteriorController") as! ExteriorController
            vc.objectSelectedService = objectCategory
            navigationController?.pushViewController(vc,animated: true)
        }
    }
    
    @IBAction func didTapExpressDetails(_ sender: UIButton) {
        if let objectCategory = arrCategories[sender.tag] {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ExteriorController") as! ExteriorController
            vc.objectSelectedService = objectCategory
            navigationController?.pushViewController(vc,animated: true)
        }
    }
    @IBAction func didTapServices(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ServiceController") as! ServiceController
        navigationController?.pushViewController(vc,animated: true)
    }
    
    
    func CategoriesWebServices()  {
        let successClosure: DefaultResultArrayAPISuccessClosure = {
            (result) in
            self.arrCategories.removeAll()
            for res in result {
                self.arrCategories.append(Categories(value: res))
            }
            Utility.hideLoader()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.getCategoriesWith(success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
           // Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
        })
    }
    
}


extension NSNotification.Name {
    public static let CreateWashNotificationKey = NSNotification.Name(rawValue: "CreateWashNotificationKey")
    public static let CreateWashOrderNotification = NSNotification.Name(rawValue: "CreateWashOrderNotification")
}
