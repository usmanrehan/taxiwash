//
//  AboutUsController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class AboutUsController: BaseController {

    @IBOutlet weak var txtViewAboutUs: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ABOUT US"
        if Connectivity.isConnectedToInternet() {
            CMSService(Type:"about_us")
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
       // txtViewAboutUs.contentInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func CMSService(Type:String){
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            self.txtViewAboutUs.text = (result["body"] as! String).htmlToString
            Utility.hideLoader()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.AboutUserWith(type: Type, success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    

}
