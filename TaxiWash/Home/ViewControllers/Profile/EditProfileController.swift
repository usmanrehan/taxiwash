//
//  EditProfileController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/20/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class EditProfileController: BaseController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnImagePicker: UIButton!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    
    let picker = UIImagePickerController()
    var ratingPopUp : RatingPopUp!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        self.title = "EDIT PROFILE"
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashNotificationKey, object: nil)
        self.ratingPopUp = Bundle.main.loadNibNamed("RatingPopUp", owner: self, options: nil)?.last as? RatingPopUp
        self.ratingPopUp.frame = CGRect(x: 0, y: 0 , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
        txtFieldName.text = AppStateManager.sharedInstance.loggedInUser?.full_name
        txtFieldEmail.text = AppStateManager.sharedInstance.loggedInUser?.email
        txtFieldPhoneNumber.text = AppStateManager.sharedInstance.loggedInUser?.phone
        
        if AppStateManager.sharedInstance.loggedInUser?.profile_image != nil {
            let url = URL(string:  (AppStateManager.sharedInstance.loggedInUser!.profile_image ))
            imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        } else {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    

 
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("CreateWashNotificationKey")
    }
    @objc func notificationReceived(notification:NSNotification)    {
        self.view.window?.addSubview(self.ratingPopUp)
        let orderID =  notification.userInfo?["order_id"] as! Int
        self.ratingPopUp.orderId = orderID
    }
    

    @IBAction func didTapImagePicker(_ sender: Any) {
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            print("Save")
            self.methodeChooseCamera()
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Photo Library", style: .default)
        { _ in
            print("Delete")
            self.methodeChoosePhoto()
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    // Mark :- Image Picker Control
    func methodeChooseCamera()                                                                                          {
        picker.allowsEditing = false
        picker.sourceType = .camera
        picker.cameraCaptureMode = .photo
        self.present(picker, animated: true, completion: nil)
    }
    func methodeChoosePhoto()                                                                                           {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])   {
        
        //var newImage: UIImage
        
        if let possibleImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = possibleImage
            
        } else if let possibleImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageView.image = possibleImage
            
        } else {
            return
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)                                              {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSaveProfile(_ sender: Any) {
        if Connectivity.isConnectedToInternet() {
            if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
               UpdateUserProfile()
            }
         
        } else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
            
        }
    }
    
    func UpdateUserProfile()                                 {
        let imageData = UIImageJPEGRepresentation((imageView?.image!)!, 0.5)!
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            print(result)
            
           // let token = AppStateManager.sharedInstance.loggedInUser.token
            
            AppStateManager.sharedInstance.loggedInUser = User(value: result)
            
            try! Global.APP_REALM?.write(){
               // AppStateManager.sharedInstance.loggedInUser.token = token
                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser!, update: true)
            }
            
            Utility.AlertSucess(title: "Success", Message: "Profile is successfully updated")
            _ = self.navigationController?.popViewController(animated: true)
            Utility.hideLoader()
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.editUserWith(Name: txtFieldName.text!, Email: txtFieldEmail.text!, PhoneNumber: txtFieldPhoneNumber.text!, ProfilePicture: imageData,  success: successClosure,  failure: { (error) in
            Utility.hideLoader()
            Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
            print(error.localizedFailureReason!)
        })
    }
    
}
