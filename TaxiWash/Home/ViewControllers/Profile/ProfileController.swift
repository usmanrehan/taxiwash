//
//  ProfileController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/19/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage


class ProfileController: BaseMenuController {

    
    @IBOutlet weak var labNameHeading: UILabel!
    @IBOutlet weak var labEmail: UILabel!
    @IBOutlet weak var labPhoneNumber: UILabel!
    @IBOutlet weak var imageViewProfile: UIImageView!
    
    var ratingPopUp : RatingPopUp!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "PROFILE"
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived), name: .CreateWashNotificationKey, object: nil)
        self.ratingPopUp = Bundle.main.loadNibNamed("RatingPopUp", owner: self, options: nil)?.last as? RatingPopUp
        self.ratingPopUp.frame = CGRect(x: 0, y: 0 , width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )
    }
    override func viewWillAppear(_ animated: Bool) {
        if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
            Utility.alert(title: "Alert", Message: "Please Login")
        } else {
            labNameHeading.text = AppStateManager.sharedInstance.loggedInUser?.full_name
            labEmail.text = AppStateManager.sharedInstance.loggedInUser?.email
            labPhoneNumber.text = AppStateManager.sharedInstance.loggedInUser?.phone
            labNameHeading.text = AppStateManager.sharedInstance.loggedInUser?.full_name
            
            if AppStateManager.sharedInstance.loggedInUser?.profile_image != nil {
                let url = URL(string:  (AppStateManager.sharedInstance.loggedInUser!.profile_image ))
                imageViewProfile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
            } else {
                
            }
        }
       
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("CreateWashNotificationKey")
    }
    @objc func notificationReceived(notification:NSNotification)    {
        self.view.window?.addSubview(self.ratingPopUp)
        let orderID =  notification.userInfo?["order_id"] as! Int
        self.ratingPopUp.orderId = orderID
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapEditProfile(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileController") as! EditProfileController
        navigationController?.pushViewController(vc,animated: true)
    }
    
   
}
