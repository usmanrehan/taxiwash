//
//  ChangePasswordViewController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseController {
    
    @IBOutlet weak var txtFieldCurrentPassword: UITextField!
    @IBOutlet weak var txtFieldNewPassword: UITextField!
    @IBOutlet weak var txtFieldConformPassword: UITextField!
    
     var PasswordChanged: PasswordChanged!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "CHANGE PASSWORD"
        
        self.PasswordChanged = Bundle.main.loadNibNamed("PasswordChanged", owner: self, options: nil)?.last as? PasswordChanged
        self.PasswordChanged.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height )

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func didTapUpdatePassword(_ sender: Any) {
        
        if (txtFieldCurrentPassword.text?.isEmpty)! {
            Utility.alert(title: "Alert", Message: "Please enter you current password")
        }
        else {
            if  ((txtFieldCurrentPassword.text?.count)! < 6) {
                Utility.alert(title: "Alert", Message: "Password be greater than six digits")
            }else{
                
                if (txtFieldNewPassword.text?.isEmpty)! {
                    Utility.alert(title: "Alert", Message: "New password not be empty")
                } else {
                    if ((txtFieldNewPassword.text?.count)! < 6)  {
                        Utility.showAlert(title: "Alert", message: "New password greater than six digits")
                        
                    } else {
                        if (txtFieldConformPassword.text?.isEmpty)! {
                            Utility.alert(title: "Alert", Message: "Confirm password not be empty")
                        } else {
                            if ((txtFieldConformPassword.text?.count)! < 6)  {
                                Utility.alert(title: "Alert", Message: "Confirm password greater than six digits")
                                
                            } else {
                                if txtFieldNewPassword.text == txtFieldConformPassword.text {
                                    
                                    if Connectivity.isConnectedToInternet() {
                                        if  AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                                            Utility.alert(title: "Alert", Message: "Please Login")
                                        } else {
                                             ChangePasswordWebService()
                                        }
                                    } else  {
                                        Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
                                    }
                                } else {
                                    Utility.alert(title: "Alert", Message: "Your password does not match")
                                }
                                
                                
                            }
                            
                        }
                        
                        
                    }
                }
                
                
            }
            
        }
    }
    
    func ChangePasswordWebService(){
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            Utility.hideLoader()
            print(result)
            self.view.window?.addSubview(self.PasswordChanged)
            _ = self.navigationController?.popViewController(animated: true)
            self.txtFieldNewPassword.text = nil
            self.txtFieldConformPassword.text = nil
            self.txtFieldCurrentPassword.text = nil
            
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.ChangeUserPasswordWith(OldPassword: txtFieldCurrentPassword.text!, Password: txtFieldNewPassword.text!, PasswordConfirmation: txtFieldConformPassword.text!,  success: successClosure, failure: { (error) in
            Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
            Utility.hideLoader()
        })
    }
    
}
