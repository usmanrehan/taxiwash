//
//  PasswordChanged.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class PasswordChanged: UIView {

    @IBAction func didTapPasswordChanged(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
