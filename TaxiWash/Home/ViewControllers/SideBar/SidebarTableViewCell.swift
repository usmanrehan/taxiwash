//
//  SidebarTableViewCell.swift
//  We Wash
//
//  Created by Khawar Islam on 24/08/2017.
//  Copyright © 2017 Khawar Islam. All rights reserved.
//

import UIKit

class SidebarTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewSidebar: UIImageView!
    @IBOutlet weak var labMenuSidebar: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
