//
//  SideBarController.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideBarController: LGSideMenuController {

    let preferredLanguage = Locale.preferredLanguages[0]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if preferredLanguage.contains("ar")  {
            rightViewPresentationStyle = .scaleFromBig
            rightViewWidth = UIScreen.main.bounds.size.width * 0.6
            rootViewScaleForRightView = 0.8
            
        } else if preferredLanguage.contains("en") {
            leftViewPresentationStyle = .scaleFromBig
            leftViewWidth = UIScreen.main.bounds.size.width * 0.6
            rootViewScaleForLeftView = 0.8
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
