//
//  SidebarController.swift
//  We Wash
//
//  Created by Khawar Islam on 24/08/2017.
//  Copyright © 2017 Khawar Islam. All rights reserved.
//

import UIKit
import  LGSideMenuController

class UserSidebarController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableViewSidebar: UITableView!
    
    var arrSidebarMenu = [NSLocalizedString("Home", comment: ""),
                          NSLocalizedString(" Profile", comment: ""),
                          NSLocalizedString("History", comment:""),
                          NSLocalizedString(" Notifications",comment:""),
                          NSLocalizedString("Settings",comment:""),NSLocalizedString("Logout",comment:"")]
    
    var arrSidebarMenuImages = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "history"),#imageLiteral(resourceName: "sidebarNotification"),#imageLiteral(resourceName: "settings"),#imageLiteral(resourceName: "home")]
    
    @IBOutlet weak var imageViewProfilePic: UIImageView!
    @IBOutlet weak var labName: UILabel!
    let preferredLanguage = Locale.preferredLanguages[0]
    
    override func viewDidLoad()                                                                         {
        super.viewDidLoad()
        
        tableViewSidebar.dataSource = self
        tableViewSidebar.delegate = self
        tableViewSidebar.reloadData()
        
        if AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
            arrSidebarMenu[5] = "Login"
            
        } else {
        }
        
    }
    override func viewWillAppear(_ animated: Bool)                                                      {
        
    }
    override func didReceiveMemoryWarning()                                                             {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int                 {
        return arrSidebarMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell      {
        
        let cell = tableViewSidebar.dequeueReusableCell(withIdentifier: "sideBarCell", for: indexPath) as! SidebarTableViewCell
        let view = UIView()
        cell.selectedBackgroundView = view
        cell.labMenuSidebar.text = arrSidebarMenu[indexPath.row]
        cell.imageViewSidebar.image = arrSidebarMenuImages[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)                       {
        tableViewSidebar.deselectRow(at: indexPath, animated: false)
        tableViewSidebar.reloadData()
        
        if indexPath.row == 0 {
            
            if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeNavController") as? UINavigationController {
                sideMenuController?.rootViewController = viewController
                if preferredLanguage.contains("ar") {
                    sideMenuController?.hideRightViewAnimated()
                } else if preferredLanguage.contains("en"){
                    sideMenuController?.hideLeftViewAnimated()
                    
                }
                
            }
            
        }
            
        else if indexPath.row == 1 {
            if AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ProfileNavController") as? UINavigationController {
                    sideMenuController?.rootViewController = viewController
                    //sideMenuController?.hideLeftViewAnimated()
                    if preferredLanguage.contains("ar") {
                        sideMenuController?.hideRightViewAnimated()
                    } else if preferredLanguage.contains("en") {
                        sideMenuController?.hideLeftViewAnimated()
                        
                    }
                }
            }
        }
        else if indexPath.row == 2 {
            if AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HistoryNavController") as? UINavigationController {
                    sideMenuController?.rootViewController = viewController
                    //sideMenuController?.hideLeftViewAnimated()
                    
                    if preferredLanguage.contains("ar") {
                        sideMenuController?.hideRightViewAnimated()
                    } else if preferredLanguage.contains("en") {
                        sideMenuController?.hideLeftViewAnimated()
                        
                    }
                }
            }
        } else if indexPath.row == 3 {
            if AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NotificationNavController") as? UINavigationController {
                    
                    sideMenuController?.rootViewController = viewController
                    //sideMenuController?.hideLeftViewAnimated()
                    
                    if preferredLanguage.contains("ar") {
                        sideMenuController?.hideRightViewAnimated()
                    } else if preferredLanguage.contains("en") {
                        sideMenuController?.hideLeftViewAnimated()
                        
                    }
                }
            }
        }
        else if indexPath.row == 4 {
            if AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.alert(title: "Alert", Message: "Please Login")
            } else {
                if let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SettingNavController") as? UINavigationController {
                    sideMenuController?.rootViewController = viewController
                    //sideMenuController?.hideLeftViewAnimated()
                    
                    if preferredLanguage.contains("ar") {
                        sideMenuController?.hideRightViewAnimated()
                    } else if preferredLanguage.contains("en") {
                        sideMenuController?.hideLeftViewAnimated()
                        
                    }
                }
            }
        }
            
        else if indexPath.row == 5 {
            
            
            
            if AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                Utility.LogoutUser()
                
            } else {
                
                let refreshAlert = UIAlertController(title: NSLocalizedString("Logout", comment: ""), message: NSLocalizedString("Are you sure you want to \n Logout",comment: ""),preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { (action: UIAlertAction!) in
                    self.LogoutEmployeeWebservice()
                    
                }))
                refreshAlert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: { (action: UIAlertAction!) in
                }))
                self.present(refreshAlert, animated: true, completion: nil)
            }
            
        }
        
        
    }
    
    func LogoutEmployeeWebservice()    {
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            
            print(result)
            Utility.LogoutUser()
            
            
        }
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.UserLogoutWith(success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }
    
    
}
