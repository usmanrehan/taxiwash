//
//  ResetViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/20/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ResetViewController: BaseUIViewController {
    
    //MARK:- Properties
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var continueButton: UIButton!

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueButton.cornerRadius = GISTUtility.convertToRatio(25)
        continueButton.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 25)!
    }
    
    //MARK:- Actions
    @IBAction func didTapContinue(_ sender: UIButton) {
        if let newPassword = newPasswordTextField.text, let confirmPassword = confirmPasswordTextField.text {
            if newPassword.isEmpty && confirmPassword.isEmpty {
                showToast(message: Constants.VALIDATION_ALL_FIELDS)
            }
            else if newPassword.isEmpty && !confirmPassword.isEmpty {
                showToast(message: Constants.VALIDATION_NEW_PASSWORD)
            }
            else if !newPassword.isEmpty && confirmPassword.isEmpty {
                showToast(message: Constants.VALIDATION_CONFIRM_PASSWORD)
            }
            else if !newPassword.isEmpty && !confirmPassword.isEmpty {
                if newPassword != confirmPassword {
                    showToast(message: Constants.VALIDATION_PASSWORD_MATCH)
                }
                else if newPassword.count < Constants.VALIDATION_PASSWORD_LENGTH {
                    showToast(message: Constants.VALIDATION_VALID_PASSWORD)
                }
                else {
                    processResetPassword(password: newPassword, confirmPassword: confirmPassword)
                }
            }
        }
    }
    
    //MARK:- Web Services
    func processResetPassword(password: String, confirmPassword: String) {
        
        let params: Dictionary<String, Any> = [ "password": password, "password_confirmation": confirmPassword ]
        
        Utility.showLoader()
        
        APIManager.sharedInstance.userAPIManager.resetPassword(params: params, success: { (data) in
            Utility.hideLoader()
            
            let controller = ResetSuccessfulViewController.instantiate(fromAppStoryboard: .Login)
            self.navigationController?.pushViewController(controller, animated: true)
            
        }) { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.hideLoader()
        }
        
    }
}

extension ResetViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let currentIndex = textFields.index(of: textField), currentIndex < textFields.count - 1 {
            textFields[currentIndex + 1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
