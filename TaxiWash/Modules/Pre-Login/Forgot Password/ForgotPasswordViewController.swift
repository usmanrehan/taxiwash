//
//  ForgotPasswordViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: BaseUIViewController {
    
    //MARK:- Properties
    @IBOutlet weak var phoneNoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet var selectionButtons: [UIButton]!
    @IBOutlet weak var textLabel: UILabel!
    
    static var user: User?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectionButtons.first?.isSelected = true

        textLabel.font = textLabel.font.withSize(GISTUtility.convertToRatio(17))
        continueButton.cornerRadius = GISTUtility.convertToRatio(25)
        continueButton.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 25)!
    }
    
    //MARK:- Actions
    @IBAction func didTapContinue(_ sender: UIButton) {
        
        if let phoneButton = selectionButtons.first, let phoneNo = phoneNoTextField.text, phoneButton.isSelected {
            if phoneNo.isEmpty {
                showToast(message: "Please enter phone number")
            }
            else if !phoneNo.isPhoneNumber {
                showToast(message: "Please enter valid phone number")
            }
            else if !phoneNo.isEmpty, phoneNo.count >= 9 {
                processForgotPassword(email: "", phoneNo: phoneNo)
            } else {
                showToast(message: "Phone number must be atleast 9 digits")
            }
        }
        else if let emailButton = selectionButtons.last, let email = emailTextField.text, emailButton.isSelected {
            if email.isEmpty {
                showToast(message: Constants.VALIDATION_EMAIL)
            }
            else if !Validation.isValidEmail(email) {
                showToast(message: Constants.VALIDATION_VALID_EMAIL)
            } else {
                processForgotPassword(email: email, phoneNo: "")
            }
        }
    }

    @IBAction func didTapSelection(_ sender: UIButton) {
        
        if let buttonIndex = selectionButtons.index(of: sender), !selectionButtons[buttonIndex].isSelected {
            for index in selectionButtons.indices {
                if index != buttonIndex {
                    selectionButtons[buttonIndex].isSelected = !selectionButtons[buttonIndex].isSelected
                    selectionButtons[index].isSelected = !selectionButtons[index].isSelected
                }
            }
        }
    }
    
    //MARK:- Web Services
    func processForgotPassword(email: String, phoneNo: String) {

        let params: Dictionary<String, Any> = [
            "email": email,
            "phone": phoneNo
        ]

        Utility.showLoader()
        
        APIManager.sharedInstance.userAPIManager.forgotPassword(params: params, success: { (data) in
            Utility.hideLoader()
            
            ForgotPasswordViewController.user = User(value: data)
        
            if let message = ForgotPasswordViewController.user?.message {
                let controller = CodeViewController.instantiate(fromAppStoryboard: .Login)
                controller.message = message
                self.navigationController?.pushViewController(controller, animated: true)
            }
            
        }) { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.hideLoader()
        }

    }
    
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
