//
//  CodeViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/19/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class CodeViewController: BaseUIViewController {
    
    //MARK:- Properties
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var pinTextField_1: UITextField!
    @IBOutlet weak var pinTextField_2: UITextField!
    @IBOutlet weak var pinTextField_3: UITextField!
    @IBOutlet weak var pinTextField_4: UITextField!
    
    var message: String!

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showToast(message: message)
        continueButton.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 25)!
        continueButton.cornerRadius = GISTUtility.convertToRatio(25)
    }
    
    //MARK:- Actions
    @IBAction func didTapContinue(_ sender: UIButton) {
        if let pin_1 = pinTextField_1.text, let pin_2 = pinTextField_2.text, let pin_3 = pinTextField_3.text, let pin_4 = pinTextField_4.text {
            if pin_1.isEmpty && pin_2.isEmpty && pin_3.isEmpty && pin_4.isEmpty {
                showToast(message: "Please enter code")
            }
            else if pin_1.isEmpty || pin_2.isEmpty || pin_3.isEmpty || pin_4.isEmpty {
                showToast(message: Constants.VALIDATION_ALL_FIELDS)
            }
            else if !pin_1.isEmpty && !pin_2.isEmpty && !pin_3.isEmpty && !pin_4.isEmpty {
                processVerifyCode(code: pin_1+pin_2+pin_3+pin_4 )
            }
        }
    }
    
    //MARK:- Web Services
    func processVerifyCode(code: String) {
        
        let params: Dictionary<String, Any> = [ "verification_code": code ]
        
        Utility.showLoader()
        
        APIManager.sharedInstance.userAPIManager.verifyCode(params: params, success: { (data) in
            Utility.hideLoader()
            
            let controller = ResetViewController.instantiate(fromAppStoryboard: .Login)
            self.navigationController?.pushViewController(controller, animated: true)
            
        }) { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.hideLoader()
        }
        
    }
    
}

extension CodeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1  || string.count > 0) {
            let nextTag = textField.tag + 1;
            
            if textField.tag < 5 && textField.tag > 3 {
                var nextResponder = self.stackView.viewWithTag(nextTag);
                if (nextResponder == nil) {
                    nextResponder = self.stackView.viewWithTag(1);
                }
                textField.text = string;
                textField.resignFirstResponder()
            } else {
                var nextResponder = self.stackView.viewWithTag(nextTag);
                if (nextResponder == nil) {
                    nextResponder = self.stackView.viewWithTag(1);
                }
                textField.text = string;
                nextResponder?.becomeFirstResponder();
            }
            return false;
        }
        else if ((textField.text?.count)! >= 1  || string.count == 0) {
            // on deleting value from Textfield
            let previousTag = textField.tag - 1;
            // get next responder
            var previousResponder = self.stackView.viewWithTag(previousTag);
            
            if (previousResponder == nil) {
                previousResponder = self.stackView.viewWithTag(1);
            }
            textField.text = "";
            previousResponder?.becomeFirstResponder();
            return false;
        }
        return true;
    }
}


