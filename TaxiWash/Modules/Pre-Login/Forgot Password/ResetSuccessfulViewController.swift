//
//  ResetSuccessfulViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/21/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class ResetSuccessfulViewController: UIViewController {
    
    //MARK:- Properties
    @IBOutlet weak var textLabel: UILabel!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        textLabel.font = UIFont(name: "BebasNeueBold", size: 25)!
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timeOut(sender:)), userInfo: nil, repeats: false)
        
    }
    
    //MARK:- Private Methods
    @objc func timeOut(sender : Timer) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
