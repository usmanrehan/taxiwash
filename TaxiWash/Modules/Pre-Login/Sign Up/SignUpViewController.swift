//
//  SignUpViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/17/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class SignUpViewController: BaseUIViewController {
    
    //MARK:- Properties
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnGender: UIButton!
    
    @IBOutlet weak var labGender: UILabel!
    
    var dropdownGender = DropDown()
    var dropdwonGenderLabel : String! = ""
    var arrGenderType = ["Male","Female"]
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        labGender.isHidden = true
        signUpButton.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 25)!
        self.dropdownGender.dataSource = arrGenderType
        self.dropdownGender.direction = .any
       self.dropdownGender.anchorView = self.btnGender
        dropdownGender.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnGender.setTitle(item, for: .normal)
            self.dropdwonGenderLabel = item
            self.labGender.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func didTapGender(_ sender: Any) {
        dropdownGender.show()
    }
    
    
    //MARK:- Private Methods
//    fileprivate func pushToHome() {
//        let userObject = User()
//        userObject.id = 1
//        userObject.user_type = "guest"
//        AppStateManager.sharedInstance.loggedInUser = User(value: userObject)
//
//        try! Global.APP_REALM?.write(){
//            Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser!, update: true)
//        }
//
//        AppDelegate.changeRootViewController()
//    }
    
    //MARK:- Actions
    @IBAction func didTapSignUp(_ sender: UIButton) {
        if let name = nameTextField.text, let email = emailTextField.text, let mobileNo = mobileNoTextField.text, let password = passwordTextField.text, let confirmPassword = confirmPasswordTextField.text {
            
            if name.isEmpty || email.isEmpty || mobileNo.isEmpty || password.isEmpty || confirmPassword.isEmpty {
                showToast(message: Constants.VALIDATION_ALL_FIELDS)
            }
            else if !email.isEmpty, !Validation.isValidEmail(email) {
                showToast(message: Constants.VALIDATION_VALID_EMAIL)
            }
            else if !mobileNo.isPhoneNumber {
                showToast(message: "Please enter valid phone number")
            }
            else if mobileNo.count < 9 {
                showToast(message: "Phone number must be atleast 9 digits")
            }
            else if password != confirmPassword {
                showToast(message: Constants.VALIDATION_PASSWORD_MATCH)
            }
            else if !password.isEmpty, password.count < Constants.VALIDATION_PASSWORD_LENGTH {
                showToast(message: Constants.VALIDATION_VALID_PASSWORD)
            }
            else if !confirmPassword.isEmpty, confirmPassword.count < Constants.VALIDATION_PASSWORD_LENGTH {
                showToast(message: Constants.VALIDATION_VALID_PASSWORD)
            }
            else if !name.isEmpty && !email.isEmpty && !mobileNo.isEmpty && !password.isEmpty && !confirmPassword.isEmpty {
                processRegisterUser(name: name, email: email, phone: mobileNo, password: password, Gender: dropdwonGenderLabel, confirmPassword: confirmPassword)
            }
        }
    }
    
   
    
    @IBAction func didTapLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Webservices
    func processRegisterUser(name: String, email: String, phone: String, password: String, Gender: String, confirmPassword: String) {
        let params: Dictionary<String, Any> = [
            "full_name": name,
            "email": email,
            "phone": phone,
            "password": password,
            "password_confirmation": confirmPassword,
            "device_token": Constants.device_token,
            "gender" : Gender,
            "device_type": "ios"
        ]
        
        Utility.showLoader()
        
        APIManager.sharedInstance.userAPIManager.registerUser(params: params, success: { (data) in
            Utility.hideLoader()
            
            LoginViewController.shared.loginSuccessful(object: User(value: data))

        }, failure: { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.hideLoader()
        })
    }

}

extension SignUpViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let currentIndex = textFields.index(of: textField), currentIndex < textFields.count - 1 {
            textFields[currentIndex + 1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
