//
//  LoginViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/16/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: UIViewController {
    
    //MARK:- Properties
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var signInButton: UIButton!
    
    static let shared = LoginViewController()

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Connectivity.isConnectedToInternet() {
            processGetGuestToken()
        }
        else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
        signInButton.cornerRadius = GISTUtility.convertToRatio(25)
        signInButton.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 25)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        emailTextField.text = nil
        passwordTextField.text = nil
    }
    
    //MARK:- Private Methods
    func loginSuccessful(object: User) {
        AppStateManager.sharedInstance.loggedInUser = object
        
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser!, update: true)
        }
        
       Constants.APP_DELEGATE.changeRootViewController()
    }
    
    //MARK:- Actions
    @IBAction func didTapSignIn(_ sender: UIButton) {
        
        if let email = emailTextField.text, let password = passwordTextField.text {
            if email.isEmpty && password.isEmpty {
                showToast(message: Constants.VALIDATION_ALL_FIELDS)
            }
            else if email.isEmpty && !password.isEmpty {
                showToast(message: Constants.VALIDATION_EMAIL)
            }
            else if !email.isEmpty && password.isEmpty {
                if !Validation.isValidEmail(email) {
                    showToast(message: Constants.VALIDATION_VALID_EMAIL)
                } else {
                    showToast(message: Constants.VALIDATION_PASSWORD)
                }
            }
            else if !email.isEmpty && !password.isEmpty {
                if !Validation.isValidEmail(email) {
                    showToast(message: Constants.VALIDATION_VALID_EMAIL)
                }
                else if password.count < Constants.VALIDATION_PASSWORD_LENGTH {
                    showToast(message: Constants.VALIDATION_VALID_PASSWORD)
                }
                else {
                    if Connectivity.isConnectedToInternet() {
                          processLogin(email: email, password: password)
                    }
                    else  {
                        Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
                    }
                }
            }
        }
    }
    
    @IBAction func didTapGuestUser(_ sender: UIButton) {
        if Connectivity.isConnectedToInternet() {
             LoginGuestWebService()
        }
        else  {
            Utility.alert(title: NSLocalizedString("Alert", comment: ""), Message: NSLocalizedString("Internet is not Connected", comment: ""))
        }
    }
    
    func LoginGuestWebService(){
        let successClosure: DefaultDictionaryAPISuccessClosure = {
            (result) in
            Utility.hideLoader()
            AppStateManager.sharedInstance.loggedInUser = User(value: result)
            try! Global.APP_REALM?.write(){
                AppStateManager.sharedInstance.loggedInUser.user_type = "Guest"
                AppStateManager.sharedInstance.loggedInUser.id = 2
                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
            }
            if AppStateManager.sharedInstance.loggedInUser.id == 2 {
                Constants.APP_DELEGATE.changeRootViewController()
            }
        }
        
        Utility.showLoader()
        APIManager.sharedInstance.homeAuthenticationManagerAPI.GuestUserWithWith(ProjectName: "taxiwash", success: successClosure, failure: { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        })
    }

    
    @IBAction func didTapForgotPassword(_ sender: UIButton) {
        let controller = ForgotPasswordViewController.instantiate(fromAppStoryboard: .Login)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func didTapSignUp(_ sender: UIButton) {
        let controller = SignUpViewController.instantiate(fromAppStoryboard: .Login)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- Web Services
    func processLogin(email: String, password: String) {
        
        let params: Dictionary<String, Any> = [
            "email": email,
            "password": password,
            "device_token": Constants.device_token,
            "device_type" : "ios"
            
        ]
        
        print(params)
        Utility.showLoader()
        
        APIManager.sharedInstance.userAPIManager.login(params: params, success: { (data) in
            Utility.hideLoader()
            
            self.loginSuccessful(object: User(value: data))
            
        }) { (error) in
           // Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.alert(title: "Alert", Message: error.localizedFailureReason!)
            Utility.hideLoader()
        }
    
    }

    func processGetGuestToken() {
        let params: Dictionary<String, Any> = ["project_name": "taxiwash"]
        
        Utility.showLoader()
        
        APIManager.sharedInstance.homeAuthenticationManagerAPI.getToken(params: params, success: { (data) in
            Utility.hideLoader()
            
            if let token = data["token"] as? String {
                AppStateManager.sharedInstance.guestToken = token
            }
            
        }) { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.hideLoader()
        }
    }

}

extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let currentIndex = textFields.index(of: textField), currentIndex < textFields.count - 1 {
            textFields[currentIndex + 1].becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
