//
//  TutorialViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/15/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import PVOnboardKit

class TutorialViewController: UIViewController {
    
    //MARK:- Properties
    @IBOutlet weak var introView: OnboardView!
    @IBOutlet weak var skipButton: UIButton!
    
    private (set) lazy var model: IntroModel! = {
        return IntroModel()
    }()

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        skipButton.titleLabel?.font = UIFont(name: "BebasNeueBold", size: 25)!
        skipButton.cornerRadius = GISTUtility.convertToRatio(25)
        
        setUpOnboard()
    }
    
    //MARK:- Private Methods
    fileprivate func setUpOnboard() {
        introView.dataSource = self

        introView.dotImage = #imageLiteral(resourceName: "indicator_unselected")
        introView.currentDotImage = #imageLiteral(resourceName: "indicator_selected")
        introView.dotSize = CGSize(width: 8, height: 8)
        introView.reloadData()
    }
    
    //MARK:- Actions
    @IBAction func didTapSkip(_ sender: UIButton) {
        let controller = LoginViewController.instantiate(fromAppStoryboard: .Login)
        let navController = UINavigationController(rootViewController: controller)
        controller.modalTransitionStyle = .flipHorizontal
        present(navController, animated: true, completion: nil)
    }
}

extension TutorialViewController: OnboardViewDataSource {

    func numberOfPages(in onboardView: OnboardView) -> Int {
        return model.numberOfPages()
    }

    func onboardView(_ onboardView: OnboardView, viewForPageAtIndex index: Int) -> UIView {
        let pageModel = model.pageModel(forPageAtIndex: index)

        let page = IntroPage()
        page.title = pageModel.title
        page.subtitle = pageModel.description
        return page
    }
}

