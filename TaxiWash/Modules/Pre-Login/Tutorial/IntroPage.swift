/*
 IntroPage.swift
 PVOnboardKitExample
 
 Copyright 2017 Victor Peschenkov
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */


import UIKit
import PVOnboardKit

class IntroPage: UIView {
    public var title: NSAttributedString! {
        get {
            return titleLabel.attributedText
        }
        
        set {
            titleLabel.attributedText = newValue
        }
    }
    
    public var subtitle: String! {
        get {
            return subtitleLabel.text
        }
        
        set {
            subtitleLabel.text = newValue
        }
    }
    
    
    private var titleLabel: UILabel!
    private var subtitleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont(name: "BebasNeueBold", size: GISTUtility.convertToRatio(30))
        titleLabel.textColor = UIColor.white
        self.addSubview(titleLabel)
        
        subtitleLabel = UILabel()
        subtitleLabel.numberOfLines = 0
        subtitleLabel.textAlignment = .center
        subtitleLabel.lineBreakMode = .byWordWrapping
        subtitleLabel.font = UIFont(name: "Poppins-Regular", size: GISTUtility.convertToRatio(17))
        subtitleLabel.textColor = UIColor.gray
        self.addSubview(subtitleLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let titleLabelWidthInsets = CGFloat(0.0)
        let titleLabelSize = titleLabel.sizeThatFits(CGSize(
            width: GISTUtility.convertToRatio(bounds.size.width - titleLabelWidthInsets),
            height: CGFloat.greatestFiniteMagnitude
        ))
        
        titleLabel.frame = CGRect(
            x: (bounds.size.width - titleLabelSize.width) / 2.0,
            y: GISTUtility.convertToRatio(10),
            width: titleLabelSize.width,
            height: titleLabelSize.height
        )
        
        let subtitleLabelWidthInsets = GISTUtility.convertToRatio(CGFloat(120.0))
        let subtitleLabelSize = subtitleLabel.sizeThatFits(CGSize(
            width: bounds.size.width - subtitleLabelWidthInsets,
            height: CGFloat.greatestFiniteMagnitude
        ))
        
        self.subtitleLabel.frame = CGRect(
            x: (bounds.size.width - subtitleLabelSize.width) / 2.0,
            y: GISTUtility.convertToRatio(75),
            width: subtitleLabelSize.width,
            height: subtitleLabelSize.height);
    }
}
