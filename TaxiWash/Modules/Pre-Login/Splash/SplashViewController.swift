//
//  SplashViewController.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/20/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    //MARK:- Properties
    @IBOutlet weak var progressView: UIProgressView!
    
    //MARK:- Declarations
    var timer = Timer()

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(splashLoading(sender:)), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(splashTimeOut(sender:)), userInfo: nil, repeats: false)
    }
    
    //MARK:- Private Methods
    @objc func splashLoading(sender : Timer) {
        progressView.progress += 0.05
    }

    @objc func splashTimeOut(sender : Timer) {
        processGetGuestToken()
        
        if UserDefaults.standard.string(forKey: "is_new") == nil {
            UserDefaults.standard.set("false", forKey: "is_new")
            let controller = TutorialViewController.instantiate(fromAppStoryboard: .Login)
            present(controller, animated: true, completion: nil)
        } else {
           Constants.APP_DELEGATE.changeRootViewController() 
        }
    }
    
    func processGetGuestToken() {
        let params: Dictionary<String, Any> = ["project_name": "taxiwash"]
        
        Utility.showLoader()
        
        APIManager.sharedInstance.userAPIManager.getToken(params: params, success: { (data) in
            Utility.hideLoader()
            
            if let token = data["token"] as? String {
                AppStateManager.sharedInstance.guestToken = token
            }
            
        }) { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
            Utility.hideLoader()
        }
    }

    
}
