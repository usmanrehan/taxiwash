//
//  Utility.swift
//  Template
//
//  Created by Muzamil Hassan on 02/01/2017.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import SwiftMessages

import NVActivityIndicatorView

class Utility {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }
    static func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    
    static func showAlert(title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
    static func LogoutUser() {
        // User logged out
        if AppStateManager.sharedInstance.isUserLoggedIn() {

            try! Global.APP_REALM?.write(){
                AppStateManager.sharedInstance.loggedInUser = nil

                let user = Global.APP_REALM?.objects(User.self).first
                Global.APP_REALM?.delete(user!)

                Constants.APP_DELEGATE.changeRootViewController()

            }
        } else {
            Constants.APP_DELEGATE.changeRootViewController()

        }
    }

    
    
    static func formatDateAndTimeOnly(time:String , formate:String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ss"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        // 🇲🇲
        let serverTime = time
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = formate
            return result
        }()
        
        let formatedTime = localDateFormatter.string(from: localTime)
        return formatedTime
    }
    
    static func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballScale, color: AppConstants.HomeLogoColor, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    static func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    static func getCurrentTime() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    static func getCurrentDate() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!)
        return today_string
        
    }
    
    static func formatTimeOnly(time:String , formate:String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "HH:mm:ss"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        // 🇲🇲
        let serverTime = time
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = formate
            return result
        }()
        
        let formatedTime = localDateFormatter.string(from: localTime)
        return formatedTime
    }
    
    static func formatDateOnly(time:String , formate:String) -> String {
        
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "dd-MMM-yyyy"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        // 🇲🇲
        let serverTime = time
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = formate
            return result
        }()
        
        let formatedTime = localDateFormatter.string(from: localTime)
        return formatedTime
    }
    
    static func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    static func alert(title: String, Message: String) {
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.error)
        var config = SwiftMessages.Config()
        view.configureDropShadow()
        view.configureContent(title: title, body: Message)
        view.bodyLabel?.font = UIFont(name: "Poppins-Regular",size: 12.0)
        view.buttonTapHandler = { _ in SwiftMessages.hide() }
        view.button?.setTitle("Close", for: .normal)
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        SwiftMessages.show(config: config,view: view)
        
    }
    
    
    static func alertBeta(title: String, Message: String) {
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.info)
        view.configureDropShadow()
        //let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: Message)
        view.bodyLabel?.font = UIFont(name: "Poppins-Regular",size: 12.0)
        view.buttonTapHandler = { _ in SwiftMessages.hide() }
        view.button?.setTitle("Close", for: .normal)
       // view.button?.tintColor = UIColor.darkGray
        SwiftMessages.show(view: view)
        
    }
    
    
    static func AlertSucess(title: String, Message: String) {
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.success)
        view.configureDropShadow()
        //let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: Message)
        view.bodyLabel?.font = UIFont(name: "Poppins-Regular",size: 12.0)
        view.buttonTapHandler = { _ in SwiftMessages.hide() }
        view.button?.setTitle("Close", for: .normal)
        // view.button?.tintColor = UIColor.darkGray
        SwiftMessages.show(view: view)
        
    }
    
    static func AlertInfo(title: String, Message: String) {
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.info)
        view.configureDropShadow()
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: Message, iconText: iconText)
        view.bodyLabel?.font = UIFont(name: "Poppins-Regular",size: 11.0)
        view.buttonTapHandler = { _ in SwiftMessages.hide() }
        view.button?.setTitle("Close", for: .normal)
        view.configureTheme(backgroundColor:  AppConstants.HomeLogoColor, foregroundColor: UIColor.white)
        SwiftMessages.show(view: view)
        
    }
    
    
    static func applyBlurEffectToView(toView: UIView) -> UIView? {
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            toView.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = toView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            toView.addSubview(blurEffectView)
            
            return blurEffectView
        } else {
            toView.backgroundColor = UIColor.black
            return nil
        }
    }
    
    static func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}

extension UIColor {
    static var DriveNavigationBarTextColor : UIColor {
        return UIColor(red: 72/255, green: 98/255, blue: 124/255, alpha: 1)
    }
}
