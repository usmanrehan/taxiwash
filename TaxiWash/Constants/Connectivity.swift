//
//  Connectivity.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/26/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
