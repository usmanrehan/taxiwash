//
//  AppConstants.swift
//  Template
//
//  Created by Muzamil Hassan on 02/01/2017.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//
import UIKit
import Foundation
struct AppConstants{
    
    static let grayBarColor = UIColor(red: (94/255), green: (132/255), blue: (146/255), alpha: (255/255))
    static let pinkBarColor = UIColor(red: (197/255), green: (177/255), blue: (224/255), alpha: (255/255))
    static let greenBarColor = UIColor(red: (120/255), green: (190/255), blue: (119/255), alpha: (255/255))
    
    static let lightGrayTitleColor = UIColor(red: (54/255), green: (84/255), blue: (97/255), alpha: (255/255))
    static let lightGreenTitleColor = UIColor(red: (46/255), green: (192/255), blue: (202/255), alpha: (255/255))
    static let lightGrayLineColor = UIColor(red: (212/255), green: (227/255), blue: (232/255), alpha: (255/255))
       static let appWhiteColor = UIColor(red: (255/255), green: (255/255), blue: (255/255), alpha: (255/255))
    static let appTitleColor = UIColor(red: (17/255), green: (55/255), blue: (89/255), alpha: 1.0)
   
    
    // TaxiWash
    static let HomeLogoColor = UIColor(red: (242/255), green: (183/255), blue: (27/255), alpha: 1.0)
    static let HistoryBackgournColor = UIColor(red: (28/255), green: (33/255), blue: (47/255), alpha: 1.0)
    
    static let appBtnColor = UIColor(red: (43/255), green: (159/255), blue: (232/255), alpha: 1.0)
    
    static let appSideBarColor = UIColor(red: (43/255), green: (159/255), blue: (232/255), alpha: 1.0)
    
     static let AppStatusColor = UIColor(red: (88/255), green: (127/255), blue: (182/255), alpha: 1.0)
    
      static let tabarSelectionColor = UIColor(red: (46/255), green: (174/255), blue: (238/255), alpha: 1.0)
}
