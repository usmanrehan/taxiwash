//
//  HomeAuthenticationAPIManager.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/21/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HomeAuthenticationAPIManager: APIManagerBase {
    
    
    func GuestUserWithWith(ProjectName:String,success:@escaping DefaultDictionaryAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.GetToken.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "project_name":ProjectName,
            ]
        print(parameters)
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
        
    }
    
    func CancelInProgressOrder(OrderID:Int,
                               success:@escaping DefaultDictionaryAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.CancelOrder.rawValue)!
        print(route)
        let parameters: Parameters = [
            "order_id":OrderID,
            ]
        print(parameters)
        
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
    }
    
    
    func ChangeUserPasswordWith(OldPassword:String, Password: String, PasswordConfirmation:String,
                                success:@escaping DefaultDictionaryAPISuccessClosure,
                                failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: URL = POSTURLforRoute(route: Route.UpdatePassword.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "old_password":OldPassword,
            "password" : Password,
            "password_confirmation" : PasswordConfirmation
        ]
        print(parameters)
        
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }
    func postRequestUserParameter(route: String, parameters: Parameters, success:@escaping DefaultDictionaryAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure)
    {
        print(route)
        print(parameters)
        let route: URL = POSTURLforRoute(route: route)!
        print(route)
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        //         self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func UserLogoutWith(success:@escaping DefaultDictionaryAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = GETURLfor(route: Route.Logout.rawValue)!
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    func getToken(params: Dictionary<String, Any>, success: @escaping DefaultDictionaryAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.GetToken.rawValue)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    
    
    func getCategoriesWith(success:@escaping DefaultResultArrayAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        let route: URL = GETURLfor(route: Route.GetCategories.rawValue)!
        print(route)
        self.getRequestForArrayWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    func getServicesWith(success:@escaping DefaultResultArrayAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        let route: URL = GETURLfor(route: Route.GetServices.rawValue)!
        print(route)
        self.getRequestForArrayWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    func CreateOrderWith(CategoryID: Int, Date:String,Time: String,PaymentType:String, Location : String, Latitude: String,Longitude: String,  Total: String, ServiceIDs: String,
                         success:@escaping DefaultDictionaryAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: URL = POSTURLforRoute(route: Route.CreateOrder.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "category_id":CategoryID,
            "date" : Date,
            "time" : Time,
            "payment_type": PaymentType,
            "location" : Location,
            "latitude" : Latitude,
            "longitude" : Longitude,
            "total": Total,
            "services_ids" : ServiceIDs
            ]
        print(parameters)
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }
    
    func CreateOrderRequestGuestWith(CategoryID: Int, Date:String,Time: String,PaymentType:String, Location : String, Latitude: String,Longitude: String,  Total: String, ServiceIDs: String, GuestUsername: String, GuestPhone: String,
                    success:@escaping DefaultDictionaryAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: URL = POSTURLforRoute(route: Route.CreateOrderGuest.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "category_id":CategoryID,
            "date" : Date,
            "time" : Time,
            "payment_type": PaymentType,
            "location" : Location,
            "latitude" : Latitude,
            "longitude" : Longitude,
            "total": Total,
            "services_ids" : ServiceIDs,
            "guest_username": GuestUsername,
            "guest_phone" : GuestPhone
        ]
        print(parameters)
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }
    
    func OrderFeedback(OrderID:Int, Rate:Int,
                 success:@escaping DefaultDictionaryAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: URL = POSTURLforRoute(route: Route.OrderFeedback.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "order_id":OrderID,
            "rate" : Rate,
        ]
        print(parameters)
        
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }
    
    func getOrderHistory(success:@escaping DefaultDictionaryAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = GETURLfor(route: Route.GetOrderHistory.rawValue)!
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    
    func CancelOrder(OrderID:Int,
                       success:@escaping DefaultDictionaryAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: URL = POSTURLforRoute(route: Route.CancelOrder.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "order_id":OrderID,
            ]
        print(parameters)
        
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }
    
    func AboutUserWith(type: String,
                      success:@escaping DefaultDictionaryAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = GETURLfor(route: Route.CmsServices.rawValue + "?type=\(type)")!
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    
    func ChangePushNotificationStatus(PushStatus:Int,success:@escaping DefaultDictionaryAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.ChangePushNotification.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "push_status":PushStatus,
            ]
        print(parameters)
        
        self.postRequestDictionaryWith(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }
    
    func GetNotification(success:@escaping DefaultResultArrayAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        let route: URL = GETURLfor(route: Route.GetNotification.rawValue)!
        print(route)
        self.getRequestForArrayWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    func UnReadNotificationCount(success:@escaping DefaultDictionaryAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = GETURLfor(route: Route.UnreadNotification.rawValue)!
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    func editUserWith(Name: String, Email:String, PhoneNumber: String,ProfilePicture: Data,
                      success:@escaping DefaultDictionaryAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: URL = POSTURLforRoute(route: Route.EditProfile.rawValue)!
        print(route)
        
        let parameters: Parameters = [
            "full_name":Name,
            "email" : Email,
            "phone" : PhoneNumber,
            "profile_picture" : ProfilePicture
        ]
        print(parameters)
        
        self.postRequestWithMultipart(route: route, parameters: parameters, success: success, failure: failure, withHeader: true)
        
    }

    

}
