
import UIKit


typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultDictionaryAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultResultArrayAPISuccessClosure = (Array<AnyObject>) -> Void
typealias DefaultStringResultAPISuccessClosure = (String) -> Void


protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}


class APIManager: NSObject {
    

    static let sharedInstance = APIManager()
    
    var serverToken: String? {
        get {
           return AppStateManager.sharedInstance.loggedInUser?.token
        }

    }
    
    let userAPIManager = UserAPIManager()
    
    let authenticationManagerAPI = AuthenticationAPIManager()
     let homeAuthenticationManagerAPI = HomeAuthenticationAPIManager()
    
}
