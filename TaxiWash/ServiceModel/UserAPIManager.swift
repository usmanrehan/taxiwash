//
//  UserAPIManager.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/21/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import Foundation

class UserAPIManager: APIManagerBase {
    
    func getToken(params: Dictionary<String, Any>, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.GetToken.rawValue)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    
    func registerUser(params : Dictionary<String , Any>, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route : URL = POSTURLforRoute(route: Route.SignUp.rawValue)!
        self.postRequestDictionaryWith(route: route, parameters: params, success: success, failure: failure , withHeader : true)
    }
    
    func forgotPassword(params : Dictionary<String , Any>, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route : URL = POSTURLforRoute(route: Route.ForgotPassword.rawValue)!
        self.postRequestDictionaryWith(route: route, parameters: params, success: success, failure: failure , withHeader : true)
    }
    
    func verifyCode(params : Dictionary<String , Any>, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route : URL = POSTURLforRoute(route: Route.VerifyCode.rawValue)!
        self.postRequestDictionaryWith(route: route, parameters: params, success: success, failure: failure , withHeader : true)
    }
    
    func resetPassword(params : Dictionary<String , Any>, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route : URL = POSTURLforRoute(route: Route.ResetPassword.rawValue)!
        self.postRequestDictionaryWith(route: route, parameters: params, success: success, failure: failure , withHeader : true)
    }
    
    func login(params : Dictionary<String , Any>, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route : URL = POSTURLforRoute(route: Route.Login.rawValue)!
        self.postRequestDictionaryWith(route: route, parameters: params, success: success, failure: failure , withHeader : true)
    }
}
