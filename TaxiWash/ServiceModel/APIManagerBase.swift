

import UIKit
import Alamofire
import SwiftyJSON

enum Route: String {
    
    case GetToken               = "user/guestUserToken"
    case SignUp                 = "user/register"
    case ForgotPassword         = "user/forgotpassword"
    case VerifyCode             = "user/verifyCode"
    case ResetPassword          = "user/ForgotChangePassword"
    case Login                  = "user/login"
    
    //Home- Wash- TaxiWash
    case GetCategories = "wash/getcategories"
    case GetServices = "wash/getservices"
    case CreateOrder = "wash/createOrder"
      case CreateOrderGuest = "wash/createGuestUserOrder"
    case OrderFeedback = "wash/orderfeedback"
    case GetOrderHistory = "wash/getOrderHistory"
    case CancelOrder = "wash/cancelorder"
    case CmsServices = "cms/getCms"
    case ChangePushNotification = "changepushnotificationstatus"
    case GetNotification = "getnotifications"
    case UnreadNotification = "getUnreadNotificationsCount"
    //case UserLogin = "user/login"
    case EditProfile = "user/updateProfile"
     case Logout = "user/logout"
   
    

    
    case UpdatePassword = "user/changepassword"
    
 
    
    func url() -> String{
        return Constants.BaseURL + self.rawValue
    }
}

class APIManagerBase: NSObject {
    
    let baseURL = Constants.BaseURL
    //let user = User()
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let defaultError = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Request Failed."])
    
    func getAuthorizationHeader () -> Dictionary<String,String>{
        if AppStateManager.sharedInstance.isUserLoggedIn() {
            if let token = APIManager.sharedInstance.serverToken {
                return ["token":token]
            }
        }
        else if ForgotPasswordViewController.user?.token != nil {
            return ["token":ForgotPasswordViewController.user!.token]
        }
        else {
                return ["token":AppStateManager.sharedInstance.guestToken]
        }
        
        return ["Content-Type":"application/json"]
    }
    
    func getErrorFromResponseData(data: Data) -> NSError? {
        do{
            let result = try JSONSerialization.jsonObject(with: data,options: JSONSerialization.ReadingOptions.mutableContainers) as? Array<Dictionary<String,AnyObject>>
            if let message = result?[0]["message"] as? String{
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
                return error;
            }
        }catch{
            NSLog("Error: \(error)")
        }
        return nil
    }
    
    func URLforRoute(route: String,params:[String: String]) -> NSURL? {
        
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.BaseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params{
                queryItems.append(NSURLQueryItem(name:key,value: value))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            
            return components.url as NSURL?
        }
        return nil;
    }
    
    func POSTURLforRoute(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }
    
    func GETURLfor(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }
    
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure){
        Alamofire.upload (
            multipartFormData: { multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
                
        },
            to: route,
            encodingCompletion: { result in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.responseResult(response, success: {result in
                            
                            success(result as! Dictionary<String, AnyObject>)
                        }, failure: {error in
                            
                            failure(error)
                        })
                    }
                case .failure(let encodingError):
                    failure(encodingError as NSError)
                }
        }
        )
    }
    
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
                
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            
                                            success(result as! Dictionary<String, AnyObject>)
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                
                                success(result as! Dictionary<String, AnyObject>)
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
    func postRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Dictionary<String, AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    
    func postRequestWithArray(route: URL,parameters: Parameters,
                              success:@escaping DefaultStringResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResultWithNoArray(response, success: {response in
                
                success(response as String)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    
    
    func postRequestWithArrayy(route: URL,parameters: Parameters,
                               success:@escaping DefaultResultArrayAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Array<AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    
    
    func getRequestWith(route: URL,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Dictionary<String, AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
            
            
        }
    }
    
    // MARK: GetREQUEST
    
    func getRequestWithArray(route: URL,
                             success:@escaping DefaultResultArrayAPISuccessClosure,
                             failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Array<AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
            
            
        }
    }
    
    func putRequestWith(route: URL,parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .put, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Dictionary<String, AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    
    func deleteRequestWith(route: URL,parameters: Parameters,
                           success:@escaping DefaultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Dictionary<String, AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    
    //MARK: - Response Handling
    fileprivate func responseResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void
        ) {
        switch response.result
        {
        case .success:
            if let data = response.result.value {
                let dictData = data as! NSDictionary
                let errorCode:Int = (dictData["Error"] as? Int) ?? 0;
                
                if (dictData["Response"] as! String == "2000") {
                    //Success
                    success(dictData["Result"] as AnyObject)
                } else {
                    //Failure
                    let errorMessage: String = (dictData["Message"] as? String) ?? "Unknown error";
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    
                    let error = NSError(domain: "Domain", code: errorCode, userInfo: userInfo)
                    failure(error)
                    // Utility.showAlert(title: "Alert", message: error.localizedFailureReason)
                }
            } else {
                //Failure
                let errorMessage: String = "Unknown error";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                
                failure(error)
            }
            
        case .failure(let error):
            failure(error as NSError)
        }
    }
    
    // For ForgotPassword
    fileprivate func responseResultWithNoArray(_ response:DataResponse<Any>,
                                               success: @escaping (_ response: String) -> Void,
                                               failure: @escaping (_ error: NSError) -> Void
        ) {
        switch response.result
        {
        case .success:
            if let data = response.result.value {
                let dictData = data as! NSDictionary
                let errorCode:Int = (dictData["Error"] as? Int) ?? 0;
                
                if (dictData["Response"] as! String == "2000") {
                    //Success
                    success(dictData["Message"] as! String)
                } else {
                    //Failure
                    let errorMessage: String = (dictData["Message"] as? String) ?? "Unknown error";
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    
                    let error = NSError(domain: "Domain", code: errorCode, userInfo: userInfo)
                    failure(error)
                    Utility.showAlert(title: "Alert", message: error.localizedFailureReason)
                }
            } else {
                //Failure
                let errorMessage: String = "Unknown error";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                
                failure(error)
            }
            
        case .failure(let error):
            failure(error as NSError)
        }
    }
    
    fileprivate func multipartFormData(parameters: Parameters) {
        let formData: MultipartFormData = MultipartFormData()
        if let params:[String:AnyObject] = parameters as [String : AnyObject]? {
            for (key , value) in params {
                
                if let data:Data = value as? Data {
                    formData.append(data, withName: key, fileName: "fileName", mimeType: data.mimeType)
                } else {
                    formData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                }
            }
        }
    }
}


public extension Data {
    public var mimeType:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "image/jpeg";
            case 0x89:
                return "image/png";
            case 0x47:
                return "image/gif";
            case 0x49, 0x4D:
                return "image/tiff";
            case 0x25:
                return "application/pdf";
            case 0xD0:
                return "application/vnd";
            case 0x46:
                return "text/plain";
            default:
                print("mimeType for \(c[0]) in available");
                return "application/octet-stream";
            }
        }
    }
}


extension APIManagerBase
{
    // MARK: GET
    // For Array Responce
    func getRequestWithParameters(route: URL,parameters: Parameters,
                                  success:@escaping DefaultResultArrayAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure,
                                  withHeader: Bool)
    {
        
        if(withHeader)
        {
            Alamofire.request(route, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {
                    
                    response in
                    if((response as! NSArray).count == 0)
                    {
                        //                        Utility.main.showToast(message: "Record Not Found!")
                    }
                    success((response as! NSArray) as Array<AnyObject>)
                }, failure: {error in
                    
                    failure(error as NSError)
                })}
            
        }
        else
        {
            Alamofire.request(route, method: .get, parameters: parameters, encoding: URLEncoding()).responseJSON {
                response in
                
                self.responseResult(response, success: {response in
                    
                    success((response as! NSArray) as Array<AnyObject>)
                }, failure: {error in
                    
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
            }
        }
    }
    
    // For Dictionary Responce
    func getRequestForDictionaryWith(route: URL,
                                     success:@escaping DefaultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure, withHeader: Bool)
    {
        if(withHeader)
        {
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                    
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
                
            }
            
        }
        else
        {
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                    
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
            }
        }
        
    }
    
    
    func getRequestForArrayWith(route: URL,
                                success:@escaping DefaultResultArrayAPISuccessClosure,
                                failure:@escaping DefaultAPIFailureClosure, withHeader: Bool)
    {
        if(withHeader)
        {
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Array<AnyObject>)
                    
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
                
            }
            
        }
        else
        {
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Array<AnyObject>)
                    
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
            }
        }
        
    }
    // MARK: POST
    
    // For Dictionary Responce
    func postRequestDictionaryWith(route: URL,parameters: Parameters, success:@escaping DefaultAPISuccessClosure,
                                   failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        
        if(withHeader)
        {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON(completionHandler: {
                responce in
                
                self.responseResult(responce, success: {
                    response in
                    success(response as! Dictionary<String, AnyObject>)
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
            })
        }
        else
        {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    
    // For Array Responce
    func postRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultResultArrayAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure,
                         withHeader: Bool){
        
        if(withHeader)
        {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON(completionHandler: {
                responce in
                
                self.responseResult(responce, success: {
                    response in
                    success((response as! NSArray) as Array<AnyObject>/*Dictionary<String, AnyObject>*/)
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
            })
        }
        else
        {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success((response as! NSArray) as Array<AnyObject>/*Dictionary<String, AnyObject>*/)
                }, failure: {error in
                    //                    Utility.main.showToast(message: ErrorConstants.ERROR_INTERNET_CONNECTION)
                    failure(error as NSError)
                })
            }
        }
    }
    
    // For Bool Responce
    func postRequestForBooleanWith(route: URL,parameters: Parameters, success:@escaping DefaultBoolResultAPISuccesClosure,
                                   failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if(withHeader)
        {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON(completionHandler: { responce in
                
                self.responseResult(responce, success: {responce in
                    
                    success(true)
                    
                }, failure: {error in
                    
                    failure (error as NSError)})
                
            })
        }
        else
        {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(true)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    
    
}

