//
//  AppDelegate.swift
//  TaxiWash
//
//  Created by Muhammad Ashar Zia on 2/14/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import RealmSwift
import UserNotifications
import GooglePlacePicker
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate  {

    var window: UIWindow?
     var config = Realm.Configuration()
    var `Type` : String = ""
    var OrderID : String = ""
    class func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
        //changeRootViewController()
        IQKeyboardManager.sharedManager().enable = true
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font: UIFont.init(name: "BebasNeueBold", size: 24)!]
        GMSPlacesClient.provideAPIKey("AIzaSyAcJQXYHkg_MP9aBDbrNqKrbvBLc_8jf9k")
         GMSServices.provideAPIKey("AIzaSyBPnl4vTMyNXOJFsxtELqC7g3k9qlPmW0I")
        
        UNUserNotificationCenter.current().delegate = self
         registerForRemoteNotifications()
        Utility.printFonts()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func changeRootViewController() {
    
        if AppStateManager.sharedInstance.isUserLoggedIn() {
            
            if AppStateManager.sharedInstance.loggedInUser?.user_type == "user"  || AppStateManager.sharedInstance.loggedInUser.user_type == "Guest" {
                
                let storyBoard = UIStoryboard(name: "Home", bundle: nil)
                
                let homeController = storyBoard.instantiateViewController(withIdentifier: "SideBarController") as! SideBarController
                
                let right = storyBoard.instantiateViewController(withIdentifier: "UserSidebarController") as! UserSidebarController
                let left = storyBoard.instantiateViewController(withIdentifier: "UserSidebarController") as! UserSidebarController
                
                let preferredLanguage = Locale.preferredLanguages[0]
                if preferredLanguage.contains("ar") {
                    homeController.rightViewController = right
                    
                } else if preferredLanguage.contains("en")  {
                    homeController.leftViewController = left
                }
                
                UIView.transition(with: AppDelegate.sharedInstance().window!, duration: 0.5, options: .transitionFlipFromRight, animations: {
                    AppDelegate.sharedInstance().window?.rootViewController = homeController
                }, completion: nil)
            } else {
                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let SignInBaseNavigationController = storyboard.instantiateViewController(withIdentifier: "SignInBaseNavigationController")
                AppDelegate.sharedInstance().window?.rootViewController = nil;
                
                AppDelegate.sharedInstance().window?.rootViewController = SignInBaseNavigationController
            }
        }
        else {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let SignInBaseNavigationController = storyboard.instantiateViewController(withIdentifier: "SignInBaseNavigationController")
            AppDelegate.sharedInstance().window?.rootViewController = nil;
            
            AppDelegate.sharedInstance().window?.rootViewController = SignInBaseNavigationController
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        if token.count == 0 {
            token = "0"
        }
        
        Constants.device_token = token
        
        
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    
        print(response.notification.request.content.userInfo)
        
        let data = response.notification.request.content.userInfo
        guard let aps = data[AnyHashable("aps")] as? NSDictionary else {
            return
        }
        
        Type = aps["type"] as! String
        if Type == "rating" {
            guard let orderID = (aps["order_id"] as? NSString)?.intValue else {
                return
            }
            OrderID = "\(orderID)"
            let myDict = ["order_id":OrderID] as [String : Any]
            NotificationCenter.default.post(name: .CreateWashNotificationKey, object: nil, userInfo: myDict)
        }
        
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print(notification.request.content.userInfo)
        
        let data = notification.request.content.userInfo
        guard let aps = data[AnyHashable("aps")] as? NSDictionary else {
            return
        }
        Type = aps["type"] as! String
        
        print(aps)
        if Type == "rating" {
            
            guard let orderID = (aps["order_id"] as? NSNumber)?.intValue else {
                return
            }
            OrderID = "\(orderID)"
            let myDict = ["order_id":orderID] as [String : Any]
            NotificationCenter.default.post(name: .CreateWashNotificationKey, object: nil, userInfo: myDict)
        }
        
        completionHandler([.alert,.sound,.badge])
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
    }
    func registerForRemoteNotifications() {
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            UIApplication.shared.registerForRemoteNotifications()
            // application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *)  {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *)  {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            UIApplication.shared.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }


    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

