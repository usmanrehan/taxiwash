//
//  AppStateManager.swift
//  Template_iOS
//
//  Created by Usman Bin Rehan on 11/2/17.
//  Copyright © 2017 Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    
    var loggedInUser: User!
    var guestToken: String!
    var realm: Realm!

    override init() {
        super.init()
        
        if(!(realm != nil)) {
            realm = try! Realm()
        }
        
        loggedInUser = realm.objects(User.self).first
    }
    
    func isUserLoggedIn() -> Bool{
        
        if (self.loggedInUser) != nil {
            if (self.loggedInUser?.isInvalidated)! {
                return false
            }
            return true
        }
        return false
    }
    
 
}
