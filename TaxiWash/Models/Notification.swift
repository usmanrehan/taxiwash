//
//  Notification.swift
//  We Wash
//
//  Created by Khawar Islam on 22/09/2017.
//  Copyright © 2017 Khawar Islam. All rights reserved.
//

import UIKit
import RealmSwift

class Notification: Object {
   
   @objc dynamic var id = 0
    //dynamic var sender_id = 0
   @objc dynamic var action_id = 0
   @objc dynamic var message : String? = ""
    @objc dynamic var action_type : String? = ""
    @objc dynamic var status = 0
   @objc dynamic var created_at : String? = ""
    
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
