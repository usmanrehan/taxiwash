//
//  CategoryServices.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/21/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import RealmSwift

class CategoryServices: Object {
  
    @objc dynamic var id = 0
    @objc dynamic var category_id : String = ""
    @objc dynamic var service_id : String = ""
    @objc dynamic var service_detail : ServiceDetail?
    var category_services = List<CategoryServices>()
}
