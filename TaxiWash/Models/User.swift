//
//  User.swift
//  AlbaCars
//
//  Created by Asad Rehman on 09/12/2016.
//  Copyright © 2016 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

class User: Object {

    @objc dynamic var token : String = ""
    @objc dynamic var is_active = 0
    @objc dynamic var full_name : String! = ""
    @objc dynamic var email : String = ""
    @objc dynamic var profile_image : String = ""
    @objc dynamic var push_status : Int = 0
    @objc dynamic var verification_code: String = ""
    @objc dynamic var role_id = 0
    @objc dynamic var id = 0
    @objc dynamic var is_deleted = 0
    @objc dynamic var device_token : String = ""
    @objc dynamic var device_type : String = ""
    @objc dynamic var created_at : String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var user_type : String! = "user"
    @objc dynamic var profile_picture : String = ""
    @objc dynamic var message : String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }    
    
}
