//
//  Categories.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/21/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import RealmSwift

class Categories: Object {
    
    @objc dynamic var flag = false
    @objc dynamic var id = 0
    @objc dynamic var price : String! = ""
    @objc dynamic var title : String?
    @objc dynamic var category_image : String?
    @objc dynamic var detail : String?
     @objc dynamic var service_image : String?
    var category_services = List<CategoryServices>()
}
