//
//  ServiceDetail.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/21/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import RealmSwift

class ServiceDetail: Object {
    
    var flag = false
    @objc dynamic var id = 0
    @objc dynamic var title : String = ""
    @objc dynamic var price : String = ""
      @objc dynamic var detail : String = ""
    @objc dynamic var service_detail : ServiceDetail?
    var service_images = List<Categories>()
    override static func primaryKey() -> String? {
        return "id"
    }
}
