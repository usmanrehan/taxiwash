//
//  History.swift
//  TaxiWash
//
//  Created by Khawarislam on 2/23/18.
//  Copyright © 2018 Muhammad Ashar Zia. All rights reserved.
//

import UIKit
import RealmSwift

class History: Object {
    
   @objc dynamic var id = 0
   @objc dynamic var date: String = ""
   @objc dynamic var time : String = ""
   @objc dynamic var location : String = ""
    var category_detail : Categories? = nil
    var order_services = List<CategoryServices>()

}
